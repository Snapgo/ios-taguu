//
//  PQRSViewController.swift
//  autogestion
//
//  Created by Asesoftware on 10/29/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//
import Alamofire
import SwiftyJSON
import UIKit

class PQRSViewController: UIViewController, UITextViewDelegate {
    
    
    @IBOutlet weak var ico_borrar_1: UIImageView!
    
    @IBOutlet weak var ico_borrar_2: UIImageView!
    @IBOutlet weak var ico_borrar_3: UIImageView!
    @IBOutlet weak var photoThree: UIImageView!
    @IBOutlet weak var photoTwo: UIImageView!
    @IBOutlet weak var photoOne: UIImageView!
    @IBOutlet weak var descriptionPqrs: UITextView!
    @IBOutlet weak var pqrButt: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
           let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .portrait
        ico_borrar_1.isHidden = true
        ico_borrar_2.isHidden = true
        ico_borrar_3.isHidden = true

        
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
    descriptionPqrs.delegate = self
        pqrButt.isEnabled = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .portrait
        
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        
        if(CameraPqrViewController.camPqrClass.photo1 != nil){
            ico_borrar_1.isHidden = false

         photoOne.image = CameraPqrViewController.camPqrClass.photo1
        }
        else{
            ico_borrar_1.isHidden = true

            photoOne.image = #imageLiteral(resourceName: "btn_foto1")
        }
        if(CameraPqrViewController.camPqrClass.photo2 != nil){
            ico_borrar_2.isHidden = false

            photoTwo.image = CameraPqrViewController.camPqrClass.photo2
        }
        else{
            ico_borrar_2.isHidden = true

            photoTwo.image = #imageLiteral(resourceName: "btn_foto2")
        }
        if(CameraPqrViewController.camPqrClass.photo3 != nil){
            ico_borrar_3.isHidden = false

            photoThree.image = CameraPqrViewController.camPqrClass.photo3
        }
        else{
            ico_borrar_3.isHidden = true

            photoThree.image = #imageLiteral(resourceName: "btn_foto3")
        }
    
    }
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
        validteFields()    }

    func validteFields(){
        if(descriptionPqrs.text != ""){
            pqrButt.backgroundColor  = #colorLiteral(red: 0.4676477313, green: 0.7703307867, blue: 0.3479424715, alpha: 1)
            pqrButt.isEnabled = true
        }
        else{
            pqrButt.backgroundColor  = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            pqrButt.isEnabled = false


        }
    }
    @IBAction func pqrsButt(_ sender: Any) {
        let urlString = "http://192.168.3.20:8090/api/pqrs/registrarPqrs"
        var photo1Base = ""
        var photo2Base = ""
        var photo3Base = ""
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoadingView") as! LoadingViewController
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        present(popOverVC, animated: true)
        
        
        if(CameraPqrViewController.camPqrClass.photo1 != nil){
           photo1Base = CameraPqrViewController.camPqrClass.photo1!.resizedTo1MB()!.toBase64(format: ImageFormat.png)!
        }
        if(CameraPqrViewController.camPqrClass.photo2 != nil){
            photo2Base = CameraPqrViewController.camPqrClass.photo2!.resizedTo1MB()!.toBase64(format: ImageFormat.png)!
        }
        if(CameraPqrViewController.camPqrClass.photo3 != nil){
            photo3Base = CameraPqrViewController.camPqrClass.photo3!.resizedTo1MB()!.toBase64(format: ImageFormat.png)!
        }
        
        let userName: NSString? = UserDefaults.standard.object(forKey: "UserName") as? NSString
        let userEmail: NSString? = UserDefaults.standard.object(forKey: "UserEmail") as? NSString
        let userId: NSInteger? = UserDefaults.standard.object(forKey: "UserID") as? NSInteger
         let userPhone: NSString? = UserDefaults.standard.object(forKey: "UserPhone") as? NSString
        let userDoc: NSString? = UserDefaults.standard.object(forKey: "UserDoc") as? NSString
        let userLastName: NSString? = UserDefaults.standard.object(forKey: "UserLastName") as? NSString
        
        
        
        Alamofire.request(urlString, method: .post, parameters: ["cellPhone": "\(userPhone!)",
                                                                 "descripcion": "\(descriptionPqrs.text!)",
                                                                 "email": "\(userEmail!)",
                                                                 "evidencias": [
                                                                    "frontal": "\(photo1Base)",
                                                                    "lateralDerecha": "\(photo2Base)",
                                                                    "lateralIzquierda": "\(photo3Base)"
    
            ],
                                                                 "identification": "\(userDoc!)",
                                                                 "lastNames": "\(userLastName!)",
                                                                 "names": "\(userName)"],encoding: JSONEncoding.default, headers: nil).responseJSON {
                response in
                //  vc.dismiss(animated: false, completion: nil)
                
                switch response.result {
                case .success(let Json):
                    print(response)
                    let response = Json.self as! NSDictionary
                    let responsetext = response.object(forKey: "data")!
                    print(responsetext)
                    
                    let responsesuccess = response.object(forKey: "success")!
                    if(responsesuccess as! Bool == true){
                        
                            popOverVC.dismiss(animated: false){
                        self.performSegue(withIdentifier: "toVerifyPqr", sender: sender)
                        }
                        
                    }
                    
                    break
                case .failure(let error):
                    //  vc.dismiss(animated: false, completion: nil)
                    print(error)
                }
        }
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func toCamera(_ sender: Any) {
    
        if(CameraPqrViewController.camPqrClass.photo1 == nil){ CameraPqrViewController.camPqrClass.camselected = 1
            GalleryPqrViewController.galeryClass.indexPhoto = 0

        self.performSegue(withIdentifier: "toPqrGallery", sender: sender)
        }
        else{
             self.performSegue(withIdentifier: "toPqrGalleryCam", sender: sender)
            
        }
        
    }
    

    @IBAction func toCamera2(_ sender: Any) {
        
         if(CameraPqrViewController.camPqrClass.photo2 == nil){
        CameraPqrViewController.camPqrClass.camselected = 2
            GalleryPqrViewController.galeryClass.indexPhoto = 1

        self.performSegue(withIdentifier: "toPqrGallery", sender: sender)
         }
         else{
            self.performSegue(withIdentifier: "toPqrGalleryCam", sender: sender)
            
        }
        
    }
    @IBAction func toCamera3(_ sender: Any) {
        if(CameraPqrViewController.camPqrClass.photo3 == nil){

        CameraPqrViewController.camPqrClass.camselected = 3
            GalleryPqrViewController.galeryClass.indexPhoto = 2


        self.performSegue(withIdentifier: "toPqrGallery", sender: sender)
        }
            else{
                self.performSegue(withIdentifier: "toPqrGalleryCam", sender: sender)
                
            }
    }
}
