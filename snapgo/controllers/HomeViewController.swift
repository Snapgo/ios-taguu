//
//  HomeViewController.swift
//  autogestion
//
//  Created by Asesoftware on 9/8/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit
import  Alamofire
import SwiftyJSON

class HomeViewController: UIViewController {

    
    var assisNum : String = ""
    var varadoNum : String = ""
    var hurtoNum : String = ""
    var estrelladoNum : String = ""
    struct homeClass {
        static var uniquePlaca = 0
    }

    @IBOutlet weak var names: UILabel!
    @IBOutlet weak var lastName: UILabel!
    
    @IBOutlet weak var menuCloseBack: UIButton!
    
    @IBOutlet weak var menuConstLateral: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let userLastName: NSString? = UserDefaults.standard.object(forKey: "UserLastName") as? NSString
        let userName: NSString? = UserDefaults.standard.object(forKey: "UserName") as? NSString
        self.names.text = "\(userName!)"
        self.lastName.text = "\(userLastName!)"
       getPhoneNumbersHome()
        menuCloseBack.isHidden = true
        menuConstLateral.constant = -350
        // Do any additional setup after loading the view.
    }
    

    @IBAction func callPrevisora(_ sender: Any) {
        print(assisNum)
        dialNumber(number: assisNum)
    }
    
    
    @IBAction func sendToChat(_ sender: Any) {
        
        var userDoc: NSInteger? = UserDefaults.standard.object(forKey: "userDoc") as? NSInteger
        
        var userName: NSString? = UserDefaults.standard.object(forKey: "userName") as? NSString
        var userEmail: NSString? = UserDefaults.standard.object(forKey: "userEmail") as? NSString
        
        if let url = URL(string: "https://webchat.millenium.com.co/previsora/m-entry2.jsp?username=\(userName)&tipoIdentificacion=CC&identificacion=\(userDoc)&email=\(userEmail)&question=APP") {
            UIApplication.shared.open(url)
        }
        
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
        }
    }
    
    func ToNewPlaca()  {
        self.performSegue(withIdentifier: "toNewPlaca", sender: (Any).self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        DepartmentsViewController.depReg.depName = ""
        MunicipioViewController.munReg.nameMun = ""
        
        let defaults = UserDefaults.standard
        let name = defaults.string(forKey: "userPlaca")
         if(name as? String == "true"){
            
        }
         else{
            getPlacasHome()
        }
    }
    
    
    var buttonRegpressed = false
    @IBAction func toRegSinister(_ sender: Any) {
        
        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
            buttonRegpressed = true
            getPlacasHome()
        }

            else{
            self.performSegue(withIdentifier: "toErrorConect", sender: (Any).self)
            }
    }
    var avlaiablePlaca = false
    
    func nullToNil(value : Any?) -> Any? {
        if value is NSNull {
            return nil
        } else {
            return value
        }
    }

    
    func getPlacasHome() {
        
        var userId: NSInteger? = UserDefaults.standard.object(forKey: "UserID") as? NSInteger
        print(userId as Any)
        let urlString = "http://192.168.3.20:8090/api/poliza/getvehiculos/\(userId!)"
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {
                response in
                switch response.result {
                case .success(let Json):
                    print(response)
                    let response = Json.self as! NSDictionary
                    let responseVehicles = response.object(forKey: "data")
                    if(self.nullToNil(value: responseVehicles) == nil){
                    print("responseVehicles")
                        self.ToNewPlaca()

                    }
                    else{
                    
                    let responseBlock = (responseVehicles as AnyObject).object(forKey: "vehiculos")!

                    var rewponsev = responseVehicles as! NSDictionary
                    
                    print(responseVehicles!)
                    
                    let dataArray = responseBlock as! NSArray;
                    
                    if(dataArray.count == 0){
                       self.ToNewPlaca()
                    }
                    else{

                        print(dataArray.count)
                        homeClass.uniquePlaca = dataArray.count
                        self.avlaiablePlaca = true
                        print(dataArray[0])
                            let obj = dataArray[0] as! NSDictionary
                            for (key, value) in obj {
                                print("Property: \"\(value as! String)\"")
                                print("Property: \"\(key as! String)\"")
                                
                                if(key as! String == "marca"){
                                    SelectPlacaViewController.RegSinister.marca_sinister = value as! String
                                }
                                if(key as! String == "placa"){
                                    SelectPlacaViewController.RegSinister.placa_sinister = value as! String
                                }
                            }
                        
                        if((self.avlaiablePlaca == true)&&(self.buttonRegpressed == true)){
                            self.performSegue(withIdentifier: "toRegSinister", sender: (Any).self)
                        }
                        else{
                            
                        }
                    }
                    print("Data items count: \(dataArray.count)")
                    }
                    break
                        
                case .failure(let error):
                    
                    print(error)
                }
        }
    }
    
    
    func getPhoneNumbersHome() {
        
        
        let urlString = "http://192.168.3.20:8090/api/parametros/AAPM_PARAM_001,AAPM_PARAM_002,AAPM_PARAM_003,AAPM_PARAM_004"
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {
                response in
                switch response.result {
                case .success(let Json):
                    print(response)
                    let response = Json.self as! NSDictionary
                    let responseNumber = response.object(forKey: "data")
                    let responseNumbers =  (responseNumber as AnyObject).object(forKey: "parameters")!

                    var rewponseN = responseNumbers as! NSDictionary
                    self.assisNum = rewponseN.object(forKey: "AAPM_PARAM_001") as! String
                    self.varadoNum = rewponseN.object(forKey: "AAPM_PARAM_003") as! String
                    self.hurtoNum = rewponseN.object(forKey: "AAPM_PARAM_004") as! String
                    self.estrelladoNum = rewponseN.object(forKey: "AAPM_PARAM_002") as! String
                    
                    break
                case .failure(let error):
                    
                    print(error)
                }
        }
        
    }
    
    @IBAction func meEstrelleButt(_ sender: Any) {
        print(estrelladoNum)
        dialNumber(number: estrelladoNum)

    }
    
    @IBAction func meVareButt(_ sender: Any) {
        print(varadoNum)
        dialNumber(number: varadoNum)

    }
    
    
    @IBAction func meRobaronButt(_ sender: Any) {
        print(hurtoNum)
        dialNumber(number: hurtoNum)

    }
    
    
    @IBAction func menuButt(_ sender: Any) {
        UIView.animate(withDuration: 0.5) {
            self.menuCloseBack.isHidden = false
            self.menuConstLateral.constant = 0
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func closeMenu(_ sender: Any) {
        UIView.animate(withDuration: 0.5) {
            self.menuCloseBack.isHidden = true
            self.menuConstLateral.constant = -350
            self.view.layoutIfNeeded()
        }
    }
    
}
