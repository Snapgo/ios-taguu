//
//  ConfirmViewController.swift
//  autogestion
//
//  Created by Asesoftware on 11/6/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit

class ConfirmViewController: UIViewController {

    @IBOutlet weak var labelDate: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "dd/MM/yyyy"
        let formattedDate = format.string(from: date)
        print(formattedDate)
        labelDate.text = formattedDate

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
