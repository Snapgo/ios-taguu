//
//  GalleyViewController.swift
//  autogestion
//
//  Created by Asesoftware on 9/26/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit
import ImageSlideshow

class GalleyViewController: UIViewController {
    @IBOutlet weak var sliderImage: ImageSlideshow!
    
    struct galeryClass {
        static var deleteAux = false
        static var indexPhoto = 0
        static var indexExist : [Bool]? = [false, false,false, false,false, false]
    }
    var imagesources: [ImageSource] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .portrait
    
            
            if( CameraViewController.camClass.photoPropiedadA != nil){
                imagesources.append( ImageSource(image: CameraViewController.camClass.photoPropiedadA!
                ))
            }
        if(( CameraViewController.camClass.photoPropiedadB != nil)){
            imagesources.append( ImageSource(image: CameraViewController.camClass.photoPropiedadB!
            ))
        }
        if( CameraViewController.camClass.photoCedulaA != nil){
            imagesources.append( ImageSource(image: CameraViewController.camClass.photoCedulaA!
            ))
        }
        if( CameraViewController.camClass.photoCedulaB != nil){
            imagesources.append( ImageSource(image: CameraViewController.camClass.photoCedulaB!
            ))
        }
        if( CameraViewController.camClass.photoLicenciaA != nil){
            imagesources.append( ImageSource(image: CameraViewController.camClass.photoLicenciaA!
            ))
        }
        if( CameraViewController.camClass.photoLicenciaB != nil){
            imagesources.append( ImageSource(image: CameraViewController.camClass.photoLicenciaB!
            ))
        }
        
        sliderImage?.setImageInputs(imagesources)
        sliderImage?.setCurrentPage(galeryClass.indexPhoto, animated: true)
        // Do any additional setup after loading the view.
        
    }
    

    
    @IBAction func closeGallery(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    
    
    func deletePhoto() {
        
        var photId = [Int]()

        for index in 0...sliderImage.currentPage{
            photId.removeAll()
            var aux  = 0
            for idexphoto in 0...GalleyViewController.galeryClass.indexExist!.count-1{
                if(GalleyViewController.galeryClass.indexExist![idexphoto] == true){
                    photId.append(idexphoto)
                    
                }
                
            }
        }
        print(imagesources.count)
        print(sliderImage!.currentPage)
        if(imagesources.count > 1){
        print(sliderImage!.currentPage)
        print(photId[sliderImage!.currentPage])
        if(photId[sliderImage.currentPage] == 0){
            CameraViewController.camClass.photoPropiedadA = nil
            GalleyViewController.galeryClass.indexExist![0] = false
        }
        if(photId[sliderImage.currentPage] == 1){
            CameraViewController.camClass.photoPropiedadB = nil
            GalleyViewController.galeryClass.indexExist![1] = false

            
        }
        if(photId[sliderImage.currentPage] == 2){
            CameraViewController.camClass.photoCedulaA = nil
            GalleyViewController.galeryClass.indexExist![2] = false

            
        }
        if(photId[sliderImage.currentPage] == 3){
            CameraViewController.camClass.photoCedulaB = nil
            GalleyViewController.galeryClass.indexExist![3] = false

            
        }
        if(photId[sliderImage.currentPage] == 4){
            CameraViewController.camClass.photoLicenciaA = nil
            GalleyViewController.galeryClass.indexExist![4] = false

            
        }
        if(photId[sliderImage.currentPage] == 5){
            CameraViewController.camClass.photoLicenciaB = nil
            GalleyViewController.galeryClass.indexExist![5] = false

            
        }
            
        }
        else{
            print(photId[0])
            
            if(photId[0] == 0){
                CameraViewController.camClass.photoPropiedadA = nil
                GalleyViewController.galeryClass.indexExist![0] = false

                
            }
            if(photId[0] == 1){
                CameraViewController.camClass.photoPropiedadB = nil
                GalleyViewController.galeryClass.indexExist![1] = false

                
            }
            if(photId[0] == 2){
                CameraViewController.camClass.photoCedulaA = nil
                GalleyViewController.galeryClass.indexExist![2] = false

            }
            if(photId[0] == 3){
                CameraViewController.camClass.photoCedulaB = nil
                GalleyViewController.galeryClass.indexExist![3] = false

            }
            if(photId[0] == 4){
                CameraViewController.camClass.photoLicenciaA = nil
                GalleyViewController.galeryClass.indexExist![4] = false

            }
            if(photId[0] == 5){
                CameraViewController.camClass.photoLicenciaB = nil
                GalleyViewController.galeryClass.indexExist![5] = false

            }
        }
        
        
        self.dismiss(animated: false, completion: nil)
        galeryClass.deleteAux = false
        
    }
    
    
    
    
    
    @IBAction func toDelete(_ sender: Any) {
       galeryClass.deleteAux = true
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBOutlet weak var cancelPhoto: UILabel!
    
    @IBAction func cancelDelete(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("dfdf")
        if(galeryClass.deleteAux == true){
            deletePhoto()
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        print("srgrg")
    }
}
