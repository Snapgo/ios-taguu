//
//  DepartmentsViewController.swift
//  autogestion
//
//  Created by Asesoftware on 9/16/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class DepsCollectionViewCell: UICollectionViewCell {
    
    
  
    
    @IBOutlet weak var itemDept: UILabel!
    
    
}

class DepartmentsViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    struct depReg {
        static var depName = ""
        var munGlob : Any
        static var depId = ""
    }
    var munById = [ Int: municipios ]()

    struct municipios {
        var munName: String
        var deptId: String
        var munId: String
    }
    struct depts {
        var deptName: String
        var deptId: String
    }
    
    
    var deptByID = [ Int: depts ]()
    

    
    @IBOutlet weak var depsCollection: UICollectionView!
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return deptByID.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "ItemDept"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! DepsCollectionViewCell
        cell.itemDept.text = deptByID[indexPath.item]?.deptName
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        MunicipioViewController.munReg.nameMun = ""
        depReg.depName = deptByID[indexPath.item]!.deptName
        depReg.depId = deptByID[indexPath.item]!.deptId
        var sinister = SinisterFormViewController()
        self.dismiss(animated: false, completion: nil)
        
    }
    

    
    @IBAction func closePopUp(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDepts()

        // Do any additional setup after loading the view.
    }
    

 
    func getDepts() {
        
        
        let urlString = "http://192.168.3.20:8888/api/dominios"
        
        Alamofire.request(urlString, method: .post, parameters: ["tipoDominio": "1","dominioID":""] ,encoding: JSONEncoding.default, headers: nil).responseJSON {
                response in
                switch response.result {
                case .success(let Json):
                    print(response)
                   
                    var desString = ""
                    var idDept = ""
                    var indexDept = 0
                    if let JSON = response.result.value as? NSDictionary{
                        
                        if let entries = JSON["data"] as? NSArray{
                            
                            for entry in entries {
                                
                                if let entry = entry as? NSDictionary {
                                    
                                    for (key, value) in entry {
                                        print("\(key) - \(value)")
                                        
                                       
                                        
                                        if (key as! String == "codigo"){
                                            
                                            idDept = value as! String
                                            self.deptByID[indexDept] = depts(deptName: desString, deptId: idDept)
                                            indexDept = indexDept + 1
                                        }
                                        if (key as! String == "nombre"){
                                            desString = value as! String
                                            
                                        }
                                       
                                    }
                                }
                            }
                        }
                    }
                    
                    print(self.deptByID)
                self.depsCollection.reloadData()
                    
                    
                    break
                case .failure(let error):
                    
                    print(error)
                
        }
        
        
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
