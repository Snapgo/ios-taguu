//
//  CameraPqrViewController.swift
//  autogestion
//
//  Created by Asesoftware on 11/6/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit

import CameraManager
import AVKit

class CameraPqrViewController: UIViewController {
    struct camPqrClass {
        static var camselected = 0
        static var photo1 : UIImage? = nil
        static var select1 = false
        static var photo2 : UIImage? = nil
        static var select2 = false
        static var photo3 : UIImage? = nil
        static var select3 = false
  
    }
    
    
    let cameraManager = CameraManager()
    @IBOutlet weak var imageCam: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .landscape
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        cameraManager.shouldFlipFrontCameraImage = true
        cameraManager.addPreviewLayerToView(self.imageCam)
        // Do any additional setup after loading the view.
    }
    
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    
    
    
    
    @IBAction func flashButton(_ sender: Any) {
        
        toggleFlash()
    }

    
    
    func toggleFlash() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    @IBAction func closeCam(_ sender: Any) {
    self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBOutlet weak var takePhoto: UIImageView!
    
    @IBAction func photoButton(_ sender: Any) {
        
        cameraManager.capturePictureWithCompletion({ (image, error) -> Void in
            if(camPqrClass.camselected == 1){
                GalleryPqrViewController.galeryClass.indexExist![0] = true

                camPqrClass.photo1 = image
                self.dismiss(animated: false, completion: nil)

            }
            if(camPqrClass.camselected == 2){
                GalleryPqrViewController.galeryClass.indexExist![1] = true
                camPqrClass.photo2 = image
                self.dismiss(animated: false, completion: nil)

            }
            if(camPqrClass.camselected == 3){
                GalleryPqrViewController.galeryClass.indexExist![2] = true
                camPqrClass.photo3 = image
                self.dismiss(animated: false, completion: nil)

            }
            })
        
    }
    
    
    

}
