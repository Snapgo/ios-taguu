//
//  CitiesTallerViewController.swift
//  autogestion
//
//  Created by Asesoftware on 10/8/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit


class cityTallerCollectionViewCell: UICollectionViewCell {
    
    
    
    
    @IBOutlet weak var itemDept: UILabel!
    
    
}
class CitiesTallerViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tallerMenuViewController.TalleresReg.ciudades.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "ItemDept"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! cityTallerCollectionViewCell
        cell.itemDept.text = tallerMenuViewController.TalleresReg.ciudades[indexPath.item]?.ciudad

       
        return cell
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        tallerMenuViewController.TalleresReg.indexCity = indexPath.item
       
        self.dismiss(animated: false, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func closePopUP(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
