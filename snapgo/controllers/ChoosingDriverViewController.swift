//
//  ChoosingDriverViewController.swift
//  autogestion
//
//  Created by Asesoftware on 11/5/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit
import DatePickerDialog
import SkyFloatingLabelTextField
import Alamofire
import SwiftyJSON

class ChoosingDriverViewController: UIViewController {

    var validForm = false
    @IBOutlet weak var marcaLabel: UILabel!
    @IBOutlet weak var placaLabel: UILabel!
    @IBOutlet weak var originText: SkyFloatingLabelTextField!
    
    @IBOutlet weak var destiniLabel: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ButtGetChoosingDriver.isEnabled = false
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        ValidateField()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        marcaLabel.text = SelectPlacaViewController.RegSinister.marca_sinister
        
        placaLabel.text = SelectPlacaViewController.RegSinister.placa_sinister
        ValidateField()
    }
    
    
    @IBAction func TimeButt(_ sender: Any) {
    hourPickerTapped()
        ValidateField()
    }
    
    @IBAction func DateButt(_ sender: Any) {
        datePickerTapped()
        ValidateField()

    }
    
    @IBOutlet weak var datePickerSelect: SkyFloatingLabelTextField!
   
    @IBOutlet weak var timePickerSelect: SkyFloatingLabelTextField!
    
    func datePickerTapped() {
        
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.day = 0
        let nextDate = Calendar.current.date(byAdding: .day, value: 7, to: currentDate)

        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        DatePickerDialog().show("Fecha", doneButtonTitle: "OK", cancelButtonTitle: "Cancelar", minimumDate: currentDate, maximumDate: nextDate, datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                self.datePickerSelect.text = formatter.string(from: dt)
                self.ValidateField()

            }
        }
        
    }
    
    
    func hourPickerTapped() {
        
        
        
        DatePickerDialog().show("Hora", doneButtonTitle: "OK", cancelButtonTitle: "Cancelar", datePickerMode: .time) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "h:mm a"
                self.timePickerSelect.text = formatter.string(from: dt)
                self.ValidateField()
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func ValidAddressDestiny(_ sender: Any) {
        ValidateField()
    }
    @IBAction func ValidAddressOrigin(_ sender: Any) {
        ValidateField()
    }
    
    @IBAction func ValidDate(_ sender: Any) {
        ValidateField()
    }
    
    
    @IBAction func ValidTime(_ sender: Any) {
        ValidateField()
    }
    
    @IBAction func validendTime(_ sender: Any) {
        ValidateField()
    }
    
    @IBAction func validendDate(_ sender: Any) {
        ValidateField()
    }
    
    
    func ValidateField(){
        
        if((originText.text! != "")&&(destiniLabel.text! != "")&&(datePickerSelect.text! != "")&&(timePickerSelect.text! != "")){
            
            ButtGetChoosingDriver.isEnabled = true
            ButtGetChoosingDriver.backgroundColor = #colorLiteral(red: 0.4676477313, green: 0.7703307867, blue: 0.3479424715, alpha: 1)
            validForm = true
            
        }
        else{
            ButtGetChoosingDriver.isEnabled = false
            ButtGetChoosingDriver.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            validForm = false
        }
        
        
    }

    @IBOutlet weak var ButtGetChoosingDriver: UIButton!
    
    
    @IBAction func saveConductor(_ sender: Any) {
        
        if(validForm == true){
            let urlString = "http://192.168.3.20:8090/api/axa/conductorElegido"
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoadingView") as! LoadingViewController
            popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            present(popOverVC, animated: true)
            
            let userName: NSString? = UserDefaults.standard.object(forKey: "UserName") as? NSString
            let userEmail: NSString? = UserDefaults.standard.object(forKey: "UserEmail") as? NSString
            let userId: NSInteger? = UserDefaults.standard.object(forKey: "UserID") as? NSInteger
           
            
            
            Alamofire.request(urlString, method: .post, parameters: ["apellidos": "\(userName)"
                ,"direccionDestino":"\(destiniLabel.text!)",
                "direccionOrigen":"\(originText.text!)",
                "email" : "\(userEmail!)",
                "fechaHora" : "\(datePickerSelect.text!)-\(timePickerSelect.text!)"
                ,"marca":"\(SelectPlacaViewController.RegSinister.marca_sinister)",
                "nombres":"\(String(describing: userName))",
                "placa":"\(SelectPlacaViewController.RegSinister.placa_sinister)",
                "usuarioID":"\(userId!)"],encoding: JSONEncoding.default, headers: nil).responseJSON {
                    response in
                    //  vc.dismiss(animated: false, completion: nil)
                    
                    switch response.result {
                    case .success(let Json):
                        print(response)
                        let response = Json.self as! NSDictionary
                        let responsetext = response.object(forKey: "data")!
                        print(responsetext)
                        
                        let responsesuccess = response.object(forKey: "success")!
                        if(responsesuccess as! Bool == true){
                            popOverVC.dismiss(animated: false){
                            self.performSegue(withIdentifier: "toConfirmDriver", sender: sender)
                            }
                            
                        }
                        
                        break
                    case .failure(let error):
                        //  vc.dismiss(animated: false, completion: nil)
                        print(error)
                    }
            }
        }
    }
    
    
    @IBAction func clangedTime(_ sender: Any) {
        ValidateField()
    }
    
    @IBAction func timeexit(_ sender: Any) {
        ValidateField()
    }
    
}
