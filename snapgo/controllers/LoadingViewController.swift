//
//  LoadingViewController.swift
//  autogestion
//
//  Created by Asesoftware on 10/3/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit
import SwiftGifOrigin


class LoadingViewController: UIViewController{
    


    @IBOutlet weak var viewLoading: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        viewLoading.loadGif(name: "Gif-pequeño")

    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
