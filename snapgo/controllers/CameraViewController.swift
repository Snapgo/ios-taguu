//
//  CameraViewController.swift
//  autogestion
//
//  Created by Asesoftware on 9/26/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit
import CameraManager
import AVKit

class CameraViewController: UIViewController {
    
    @IBOutlet weak var labelCam: UILabel!
    
    struct camClass {
        static var labelCamText = ""
        static var photoPropiedadA : UIImage? = nil
        static var selectPropiedadA = false
        static var photoPropiedadB : UIImage? = nil
        static var selectPropiedadB = false
        static var photoCedulaA : UIImage? = nil
        static var selectCedulaA = false
        static var photoCedulaB : UIImage? = nil
        static var selectCedulaB = false
        static var photoLicenciaA : UIImage? = nil
        static var selectLicenciaA = false
        static var photoLicenciaB : UIImage? = nil
        static var selectLicenciaB = false


    }

    let cameraManager = CameraManager()
    @IBOutlet weak var imageCam: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        labelCam.text! = camClass.labelCamText
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .landscape
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        cameraManager.shouldFlipFrontCameraImage = true
        cameraManager.addPreviewLayerToView(self.imageCam)
    

        // Do any additional setup after loading the view.
    }
    
    override var shouldAutorotate: Bool {
        return true
    }

    
    
    
    
    @IBAction func flashButton(_ sender: Any) {
        
        toggleFlash()
    }
    

    
    func toggleFlash() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBOutlet weak var takePhoto: UIImageView!
    
    @IBAction func photoButton(_ sender: Any) {
        
        cameraManager.capturePictureWithCompletion({ (image, error) -> Void in
            
            if(CameraViewController.camClass.selectPropiedadA){
                GalleyViewController.galeryClass.indexExist![0] = true
                camClass.photoPropiedadA = image
                CameraViewController.camClass.selectPropiedadA = false
            }
            if(CameraViewController.camClass.selectCedulaA){
                GalleyViewController.galeryClass.indexExist![2] = true
                camClass.photoCedulaA = image
                CameraViewController.camClass.selectCedulaA = false
            }
            
            if(CameraViewController.camClass.selectLicenciaA){
                GalleyViewController.galeryClass.indexExist![4] = true
                camClass.photoLicenciaA = image
                CameraViewController.camClass.selectLicenciaA = false
            }
            
            if(CameraViewController.camClass.selectPropiedadB){
                GalleyViewController.galeryClass.indexExist![1] = true
                camClass.photoPropiedadB = image
                CameraViewController.camClass.selectPropiedadB = false
            }
            
            if(CameraViewController.camClass.selectCedulaB){
                GalleyViewController.galeryClass.indexExist![3] = true
                camClass.photoCedulaB = image
                CameraViewController.camClass.selectCedulaB = false
            }
            if(CameraViewController.camClass.selectLicenciaB){
                GalleyViewController.galeryClass.indexExist![5] = true
                camClass.photoLicenciaB = image
                CameraViewController.camClass.selectLicenciaB = false
            }
          
            
            self.dismiss(animated: false, completion: nil)
            
        })
    }
    
    @IBAction func ClsoeCam(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
}
