//
//  MunicipioViewController.swift
//  autogestion
//
//  Created by Asesoftware on 9/16/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class MunCollectionViewCell: UICollectionViewCell {
   
    
    
    
    @IBOutlet weak var itemDept: UILabel!
    
    
}

class MunicipioViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return munById.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "ItemMun"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! MunCollectionViewCell
         cell.itemDept.text = munById[indexPath.item]?.munName
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.item)!")
        munReg.nameMun = munById[indexPath.item]!.munName
        munReg.idMun = munById[indexPath.item]!.munId
        var sinister = SinisterFormViewController()
        self.dismiss(animated: false, completion: nil)
        
    }
    
    @IBOutlet weak var munCollection: UICollectionView!
    
    
    struct munReg{
        static var nameMun = ""
        static var idMun = ""
    }
    var munById = [ Int: municipios ]()
    
    struct municipios {
        var munName: String
        var deptId: String
        var munId: String
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print(DepartmentsViewController.depReg.depId)
        getDepts()
    }
    

  
    func getDepts() {
        let urlString = "http://192.168.3.20:8888/api/dominios"
        Alamofire.request(urlString, method: .post, parameters: ["tipoDominio": "1","dominioID":"\(DepartmentsViewController.depReg.depId)"] ,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success(let response):
                if let response = response as? [String:Any],
                    let data = response["data"] as? [[String:Any]] {
                                var auxCount = 0
                                for item2 in data {
                                    var munIDs = item2["codigo"] as! String
                                    var munNames = item2["nombre"] as! String

                                    self.munById[auxCount] = municipios(munName: munNames as! String, deptId: DepartmentsViewController.depReg.depId , munId: "\(munIDs)")
                                    auxCount = auxCount + 1
                                }
                            }
                self.munCollection.reloadData()
                case .failure(let error):
                print(error)
            }
        }
    }
    
    @IBAction func closePopUp(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
}
