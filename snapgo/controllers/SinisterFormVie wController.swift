//
//  SinisterFormViewController.swift
//  autogestion
//
//  Created by Asesoftware on 9/8/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit
import Speech
import DatePickerDialog
import MaterialComponents
import AVFoundation
import Alamofire
import SwiftyJSON
import Toast_Swift
import EFCountingLabel
import SkyFloatingLabelTextField


class SinisterFormViewController: UIViewController, AVAudioRecorderDelegate, AVAudioPlayerDelegate, UITextViewDelegate {
    
    var textviewIndex = 0
 
    
    
    @IBOutlet weak var nameOther: SkyFloatingLabelTextField!
    
    @IBOutlet weak var doctypeOther: SkyFloatingLabelTextField!
    
    @IBOutlet weak var numIdOther: SkyFloatingLabelTextField!
    @IBOutlet weak var heightCheck: NSLayoutConstraint!
    @IBOutlet weak var heighSinister: NSLayoutConstraint!
    
    @IBOutlet weak var heightDocuments: NSLayoutConstraint!
    
    @IBOutlet weak var heightEvidencias: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewEvidencias: UIView!
    
    @IBOutlet weak var photoTarjetaFrente: UIImageView!
    
    @IBOutlet weak var photoCedulaFrente: UIImageView!
    
    @IBOutlet weak var photoLicenciaFrente: UIImageView!
    @IBOutlet weak var photoTarjetaReverso: UIImageView!
    @IBOutlet weak var photoCedulaReverso: UIImageView!
    @IBOutlet weak var photoLicenciaReverso: UIImageView!
    
    @IBOutlet weak var deletePropiedadFrente: UIImageView!
    @IBOutlet weak var deleteCedulaFrente: UIImageView!
    @IBOutlet weak var deleteLicenciaFrente: UIImageView!
    @IBOutlet weak var deletePropiedadReverso: UIImageView!
    @IBOutlet weak var deleteCedulaReverso: UIImageView!
    @IBOutlet weak var deleteLicenciaReverso: UIImageView!
    
    
    
    ///carSection
    
    @IBOutlet weak var PhotoLeftCar: UIImageView!
    @IBOutlet weak var deleteLeftCar: UIImageView!
    @IBOutlet weak var PhotoFrontCar: UIImageView!
    @IBOutlet weak var deleteFrontCar: UIImageView!
    @IBOutlet weak var PhotoRightCar: UIImageView!
    @IBOutlet weak var deleteRightCar: UIImageView!
    @IBOutlet weak var PhotoBackCar: UIImageView!
    @IBOutlet weak var deleteBackCar: UIImageView!
    @IBOutlet weak var PhotoFreeCar: UIImageView!
    @IBOutlet weak var deleteFreeCar: UIImageView!
    @IBOutlet weak var PhotoCroquisCar: UIImageView!
    @IBOutlet weak var deleteCroquisCar: UIImageView!
    
    
    
    ///carSection
    
    
    
    @IBOutlet weak var formViewInfoVehicle: UIView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var flightTextView: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var marcaLabel: UILabel!
    
    
    @IBOutlet weak var viewDocuments: UIView!
    
    @IBOutlet weak var verTextView: UITextView!
    @IBOutlet weak var munLabel: UILabel!
    @IBOutlet weak var deptLabel: UILabel!
    @IBOutlet weak var placaLabel: UILabel!
    
    var arrayMun = [Int.self, DepartmentsViewController.municipios.self] as! [Any] 
    var status = ""
    
    let audioEngine = AVAudioEngine()
    let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        flightTextView .delegate = self
        verTextView.delegate = self
        self.labelCount1.text = ""
        self.labelCount2.text = ""

        textViewDidChangeFlight(flightTextView)
        textViewDidChangeFlight(verTextView)

        hourTextField.text = ""
        calendarText.text = ""
        deptLabel.text = "Departamento"
        addressUser.text = ""
        verTextView.text = ""
        verTextView.placeholder = "Especifica los daños que sufrió tu vehículo…"
        flightTextView.placeholder = "Escribe tu descripción o envíanos un audio..."
        flightTextView.text = ""
        nameOther.text = ""
        doctypeOther.text = ""
        numIdOther.text = ""
        phoneuser.text = ""
        CameraViewController.camClass.photoPropiedadA = nil
        CameraViewController.camClass.photoPropiedadB = nil
        CameraViewController.camClass.photoCedulaA = nil
        CameraViewController.camClass.photoCedulaB = nil
        CameraViewController.camClass.photoLicenciaA = nil
        CameraViewController.camClass.photoLicenciaB = nil
        CameraCarViewController.camCarClass.photoBack = nil
        CameraCarViewController.camCarClass.photoCroquis = nil
        CameraCarViewController.camCarClass.photoFree = nil
        CameraCarViewController.camCarClass.photoFront = nil
        CameraCarViewController.camCarClass.photoLeft = nil
        CameraCarViewController.camCarClass.photoRight = nil
        photoTarjetaFrente.image = nil
        photoTarjetaReverso.image = nil
        photoCedulaFrente.image = nil
        photoCedulaReverso.image = nil
        photoLicenciaFrente.image = nil
        photoLicenciaReverso.image = nil
        
        
        
        deletePropiedadFrente.isHidden = true
        deletePropiedadReverso.isHidden = true
        deleteCedulaFrente.isHidden = true
        deleteCedulaReverso.isHidden = true
        deleteLicenciaFrente.isHidden = true
        deleteLicenciaReverso.isHidden = true
        
        deleteLeftCar.isHidden = true
        deleteFrontCar.isHidden = true
        deleteRightCar.isHidden = true
        deleteBackCar.isHidden = true
        deleteFreeCar.isHidden = true
        deleteCroquisCar.isHidden = true
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        

   
        setupRecorder()
        
        
        UserDefaults.standard.set("", forKey: "typedoc")
        constOtherDriver.constant =
        0
        self.nameOther.isHidden = true
        self.doctypeOther.isHidden = true
        self.numIdOther.isHidden = true
        switch SFSpeechRecognizer.authorizationStatus() {
        case .notDetermined:
            askSpeechPermission()
        case .authorized:
            self.status = "ready"
        case .denied, .restricted:
            self.status = "unavailable"
        }
        
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    //////////audio
    
    
    
    var soundRecorder : AVAudioRecorder!
    var soundPlayer : AVAudioPlayer!
    
    var fileName: String = "audioFile.m4a"
    
    
    
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func setupRecorder() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent(fileName)
        let recordSetting = [ AVFormatIDKey : kAudioFormatAppleLossless,
                              AVEncoderAudioQualityKey : AVAudioQuality.max.rawValue,
                              AVEncoderBitRateKey : 320000,
                              AVNumberOfChannelsKey : 2,
                              AVSampleRateKey : 44100.2] as [String : Any]
        
        do {
            soundRecorder = try AVAudioRecorder(url: audioFilename, settings: recordSetting )
            soundRecorder.delegate = self
            soundRecorder.prepareToRecord()
        } catch {
            print(error)
        }
    }
    
    func setupPlayer() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent(fileName)
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: audioFilename)
            soundPlayer.delegate = self
            soundPlayer.prepareToPlay()
            soundPlayer.volume = 8.0
        } catch {
            print(error)
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        playButton.isEnabled = true
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        recordButton.isEnabled = true
        playButton.setTitle("Play", for: .normal)
    }
    
    @IBAction func recordAct(_ sender: Any) {
        
        if recordButton.titleLabel?.text == "Record" {
            soundRecorder.record()
            recordButton.setTitle("Stop", for: .normal)
            playButton.isEnabled = false
        } else {
            soundRecorder.stop()
            recordButton.setTitle("Record", for: .normal)
            playButton.isEnabled = false
        }
    }
    
    @IBAction func playAct(_ sender: Any) {
        
        if playButton.titleLabel?.text == "Play" {
            playButton.setTitle("Stop", for: .normal)
            recordButton.isEnabled = false
            setupPlayer()
            soundPlayer.play()
        } else {
            soundPlayer.stop()
            playButton.setTitle("Play", for: .normal)
            recordButton.isEnabled = false
        }
    }
    
    
    
    
    

    
   //////////auido
  
    
    
    /// Ask permission to the user to access their speech data.
    func askSpeechPermission() {
        SFSpeechRecognizer.requestAuthorization { status in
            OperationQueue.main.addOperation {
                switch status {
                case .authorized:
                    self.status = "ready"
                default:
                    self.status = "unavailable"
                }
            }
    
        }
    }
    
    /// Start streaming the microphone data to the speech recognizer to recognize it live.
    func startRecording() {
        // Setup audio engine and speech recognizer
         let node = audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)
        }
        
        // Prepare and start recording
        audioEngine.prepare()
        do {
            try audioEngine.start()
            self.status = "recognizing"
        } catch {
            return print(error)
        }
        
        // Analyze the speech
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: { result, error in
            if let result = result {
                if(self.textviewIndex == 0){
                self.flightTextView.text =  " \(result.bestTranscription.formattedString)"
                }
                else{
                    self.verTextView.text = result.bestTranscription.formattedString
                }
            } else if let error = error {
                print(error)
            }
        })
    }
    
    var valueVersion2 = true

    /// Stops and cancels the speech recognition.
    
    
    func verButt(_ sender: Any){
        
    }
    
    @IBOutlet weak var labelCount1: EFCountingLabel!
    @IBOutlet weak var labelCount2: EFCountingLabel!
    
    var secondsRemaining = 30
    var secondsRemaining2 = 30
    var controlSecondV = true

    
    @IBAction func verButtonAction(_ sender: Any) {
        cancelRecording()

        self.verTextView.placeholder = ""
        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
        
        self.recordButton.isEnabled = false
            if(secondsRemaining2 >= 0){

            self.playButton.isEnabled = true
            }
        
        setupRecorder()
        textviewIndex = 1
            var valueVersion = true
            switch status {
            case "ready":
                startRecording()
                status = "recognizing"
                self.view.makeToast("Grabando")
                
                self.labelCount2.text = String(format: "%02i:%02i", 0, self.secondsRemaining2)
                controlTimersecond = true
                if(controlSecondV == true){

                Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
                    if ((self.secondsRemaining2 >= 0)&&(self.controlTimersecond == true)) {
                        print ("\(self.secondsRemaining2) seconds")
                        //self.labelCount1.text = "\(self.secondsRemaining)"
                        self.labelCount2.isHidden = false

                        self.labelCount2.text = String(format: "%02i:%02i", 0, self.secondsRemaining2)
                        
                        self.secondsRemaining2 -= 1
                        if !(self.secondsRemaining2 >= 0){
                            self.playButton.isEnabled = false
                            
                            if(self.secondsRemaining >= 1){
                                self.recordButton.isEnabled = true
                            }
                        }
                        
                    }
                }
                    controlSecondV = false
                }
                
                
                 let image = UIImage(named: "btn_grabando")
                playButton.adjustsImageWhenHighlighted = false
                playButton.setBackgroundImage(image, for: .normal)
            case "recognizing":
                self.playButton.setBackgroundImage(#imageLiteral(resourceName: "btn_audio"), for: .normal)
                self.labelCount2.isHidden = true
                controlTimersecond = false
            if(secondsRemaining >= 0){
                self.recordButton.isEnabled = true
                }
                    if(secondsRemaining2 >= 0){
                self.playButton.isEnabled = true
                }
                status = "ready"
            default:
                startRecording()

                break
            }
            valueVersion2 = false
        
      

        }
        else{
            self.performSegue(withIdentifier: "toErrorConnect", sender: sender)
        }
       
    }
    func cancelRecording() {
        audioEngine.stop()
          let node = audioEngine.inputNode
        node.removeTap(onBus: 0)
        recognitionTask?.cancel()
    }
    
    var valueVersion = true
    var controlTimerFist = true
    var controlTimersecond = true
    var controlfirstV = true
    
    @IBAction func microphonePressed() {
        
        cancelRecording()
        self.flightTextView.placeholder = ""
        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
            
                if(secondsRemaining >= 0){
            self.recordButton.isEnabled = true
            }
            self.playButton.isEnabled = false
            setupRecorder()
            textviewIndex = 0

            

            switch status {
            case "ready":
                startRecording()
                status = "recognizing"
                self.view.makeToast("Grabando")
                self.labelCount1.text = String(format: "%02i:%02i", 0, self.secondsRemaining)
                controlTimerFist = true
                if(controlfirstV == true){
                Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
                    if ((self.secondsRemaining >= 0)&&(self.controlTimerFist == true)) {
                        print ("\(self.secondsRemaining) seconds")
                        //self.labelCount1.text = "\(self.secondsRemaining)"
                        self.labelCount1.isHidden = false

                        self.labelCount1.text = String(format: "%02i:%02i", 0, self.secondsRemaining)
                        
                        self.secondsRemaining -= 1
                        if !(self.secondsRemaining >= 0){
                            self.recordButton.isEnabled = false
                        
                            if(self.secondsRemaining2 >= 1){
                                self.playButton.isEnabled = true
                            }
                        }

                    }
                }
                    controlfirstV = false
                    
                }
                
            
                self.recordButton.setBackgroundImage(#imageLiteral(resourceName: "btn_grabando"), for: .normal)
            case "recognizing":
                self.recordButton.setBackgroundImage(#imageLiteral(resourceName: "btn_audio"), for: .normal)
                controlTimerFist = false
                self.labelCount1.isHidden = true
                if !(secondsRemaining >= 1){
                self.recordButton.isEnabled = true
                }
                if (secondsRemaining2 >= 1){
                self.playButton.isEnabled = true
                }
                self.view.makeToast("Pausa")


                cancelRecording()
                status = "ready"
            default:
                break
            }
                valueVersion = false
        
      
        }
        else{

            toErorConect((Any).self)
            
        }
        
    }
    
    
    func toErorConect(_ sender: Any){
    self.performSegue(withIdentifier: "toErrorConnect", sender: sender)
    }
    

    
    override func viewDidAppear(_ animated: Bool) {
        validateFields()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .portrait
        
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        if(SelectPlacaViewController.RegSinister.placa_sinister != ""){
        self.placaLabel.text = SelectPlacaViewController.RegSinister.placa_sinister
        self.marcaLabel.text = SelectPlacaViewController.RegSinister.marca_sinister
            
            if(DepartmentsViewController.depReg.depName != ""){
              self.deptLabel.text = DepartmentsViewController.depReg.depName
            }
        
            if(MunicipioViewController.munReg.nameMun != ""){
               self.munLabel.text = MunicipioViewController.munReg.nameMun
            }else{
                self.munLabel.text = "Municipio"
            }
        
        }
        if(UserDefaults.standard.string(forKey: "typedoc") != "")
        {
            
            self.docTypeText.text = UserDefaults.standard.string(forKey: "typedoc")
            
            }
        if(CameraViewController.camClass.photoPropiedadA != nil){
            self.deletePropiedadFrente.isHidden = false
            self.photoTarjetaFrente.image = CameraViewController.camClass.photoPropiedadA
        }
        else{
            self.deletePropiedadFrente.isHidden = true

            self.photoTarjetaFrente.image = #imageLiteral(resourceName: "btn_doc_tarjeta1-1")
        }
        
        if(CameraViewController.camClass.photoCedulaA != nil){
            self.deleteCedulaFrente.isHidden = false

            if CameraViewController.camClass.photoCedulaA!.size.width < CameraViewController.camClass.photoCedulaA!.size.height{
                
                CameraViewController.camClass.photoCedulaA = imageRotatedByDegrees(oldImage: CameraViewController.camClass.photoCedulaA!, deg: 90.0)
            }

            self.photoCedulaFrente.image = CameraViewController.camClass.photoCedulaA
        }
        else{
            self.deleteCedulaFrente.isHidden = true
            
            self.photoCedulaFrente.image = #imageLiteral(resourceName: "btn_doc_cedula1-1")
        }
        if(CameraViewController.camClass.photoLicenciaA != nil){
            self.deleteLicenciaFrente.isHidden = false

            self.photoLicenciaFrente
                .image = CameraViewController.camClass.photoLicenciaA
        }
        else{
            self.deleteLicenciaFrente.isHidden = true
            
            self.photoLicenciaFrente.image = #imageLiteral(resourceName: "btn_doc_licencia1-1")
        }
        if(CameraViewController.camClass.photoPropiedadB != nil){
            self.deletePropiedadReverso.isHidden = false

            self.photoTarjetaReverso
                .image = CameraViewController.camClass.photoPropiedadB
        }
        else{
            self.deletePropiedadReverso.isHidden = true
            
            self.photoTarjetaReverso.image = #imageLiteral(resourceName: "btn_doc_tarjeta2-1")
        }
        if(CameraViewController.camClass.photoCedulaB != nil){
            self.deleteCedulaReverso.isHidden = false

            self.photoCedulaReverso
                .image = CameraViewController.camClass.photoCedulaB
        }
        else{
            self.deleteCedulaReverso.isHidden = true
            
            self.photoCedulaReverso.image = #imageLiteral(resourceName: "btn_doc_cedula2-1")
        }
        if(CameraViewController.camClass.photoLicenciaB != nil){
            deleteLicenciaReverso.isHidden = false
            self.photoLicenciaReverso
                .image = CameraViewController.camClass.photoLicenciaB
        }
        else{
            self.deleteLicenciaReverso.isHidden = true
            
            self.photoLicenciaReverso.image = #imageLiteral(resourceName: "btn_doc_licencia2-1")
        }
        
        
        
        if(CameraCarViewController.camCarClass.photoLeft != nil){
            deleteLeftCar.isHidden = false
            self.PhotoLeftCar
                .image = CameraCarViewController.camCarClass.photoLeft
        }
        else{
            self.deleteLeftCar.isHidden = true
            
            self.PhotoLeftCar.image = #imageLiteral(resourceName: "btn_ev_carro_izquierda-1")
        }
        
        
        if(CameraCarViewController.camCarClass.photoFront != nil){
            deleteFrontCar.isHidden = false
            self.PhotoFrontCar
                .image = CameraCarViewController.camCarClass.photoFront
        }
        else{
            self.deleteFrontCar.isHidden = true
            
            self.PhotoFrontCar.image = #imageLiteral(resourceName: "btn_ev_carro_frente-1")
        }
       
        
        
        
        if(CameraCarViewController.camCarClass.photoRight != nil){
            deleteRightCar.isHidden = false
            self.PhotoRightCar
                .image = CameraCarViewController.camCarClass.photoRight
        }
        else{
            self.deleteRightCar.isHidden = true
            
            self.PhotoRightCar.image = #imageLiteral(resourceName: "btn_ev_carro_derecha-1")
        }
        
        
        if(CameraCarViewController.camCarClass.photoBack != nil){
            deleteBackCar.isHidden = false
            self.PhotoBackCar
                .image = CameraCarViewController.camCarClass.photoBack
        }
        else{
            self.deleteBackCar.isHidden = true
            
            self.PhotoBackCar.image = #imageLiteral(resourceName: "btn_ev_carro_frente-1")
        }
        
        
        
        if(CameraCarViewController.camCarClass.photoFree != nil){
            deleteFreeCar.isHidden = false
            self.PhotoFreeCar
                .image = CameraCarViewController.camCarClass.photoFree
        }
        else{
            self.deleteFreeCar.isHidden = true
            
            self.PhotoFreeCar.image = #imageLiteral(resourceName: "btn_ev_libre-1")
        }
        
        
        
        if(CameraCarViewController.camCarClass.photoCroquis != nil){
            deleteCroquisCar.isHidden = false
            self.PhotoCroquisCar
                .image = CameraCarViewController.camCarClass.photoCroquis
        }
        else{
            self.deleteCroquisCar.isHidden = true
            
            self.PhotoCroquisCar.image = #imageLiteral(resourceName: "btn_ev_croquis-1")
        }
        
        validateFields()
    }
    
    
    
    
    func imageRotatedByDegrees(oldImage: UIImage, deg degrees: CGFloat) -> UIImage {
        let size = oldImage.size
        
        UIGraphicsBeginImageContext(size)
        
        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: size.width / 2, y: size.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat(M_PI / 180)))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: -1.0)
        
        let origin = CGPoint(x: -size.width / 2, y: -size.width / 2)
        
        bitmap.draw(oldImage.cgImage!, in: CGRect(origin: origin, size: size))
        
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    
    
    @IBOutlet weak var docTypeText: MDCTextField!
    @IBAction func toHour(_ sender: Any) {
        hourPickerTapped()
    }
    @IBAction func toCalendar(_ sender: Any) {
        datePickerTapped()
    }
    
    
    @IBOutlet weak var hourTextField: MDCTextField!
    func hourPickerTapped() {
        
        
        
        DatePickerDialog().show("Hora", doneButtonTitle: "OK", cancelButtonTitle: "Cancelar", datePickerMode: .time) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "h:mm a"
                self.hourTextField.text = formatter.string(from: dt)
            }
        }
    }
    
    @IBOutlet weak var calendarText: MDCTextField!
    
    func datePickerTapped() {
        
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.year = -5
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        DatePickerDialog().show("Fecha", doneButtonTitle: "OK", cancelButtonTitle: "Cancelar", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                self.calendarText.text = formatter.string(from: dt)
            }
        }
    }
    
    @IBOutlet weak var chreckButt: UISwitch!
    @IBOutlet weak var constOtherDriver: NSLayoutConstraint!
    
    @IBAction func checkRegButt(_ sender: Any) {
        
        var value = chreckButt.isOn
        print("switch value changed \(value)")
        if (value == true){
            heighSinister.constant = 828

            constOtherDriver.constant = 125
            self.nameOther.isHidden = false
            self.doctypeOther.isHidden = false
            self.numIdOther.isHidden = false
            self.deleteCroquisCar.isHidden = false

    }
        else{
            constOtherDriver.constant =
            0
            heighSinister.constant = 690
            self.nameOther.isHidden = true
            self.doctypeOther.isHidden = true
            self.numIdOther.isHidden = true
            self.deleteCroquisCar.isHidden = true


        }
    }
    var flagsinisterForm = false
    
    @IBAction func setHeightSinister(_ sender: Any) {
        
        if(flagsinisterForm == false){
            heighSinister.constant = 0
            formViewInfoVehicle.alpha = 0
            flagsinisterForm = true
            if((heightDocuments.constant == 0)&&(heightEvidencias.constant == 0)){
                heightDocuments.constant = 350
                viewDocuments.alpha = 1
                flagsinisterDocs = false
                
                
            }

        }
        else{
            
            heighSinister.constant = 818
            formViewInfoVehicle.alpha = 1
            flagsinisterForm = false
        }
        
    
        

    }
    
    @IBOutlet weak var docText: UIButton!
    @IBAction func doctypeval(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DoctypeViewController") as! DoctypeViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var buttRegSinister: UIButton!
    
    @IBAction func saveSinister(_ sender: Any) {
      
        saveFormSinister()
        
    }
    
    
    
    @IBOutlet weak var numDocOther: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneuser: SkyFloatingLabelTextField!
    
    @IBOutlet weak var addressUser: MDCTextField!
    
    var userId: NSInteger? = UserDefaults.standard.object(forKey: "UserID") as? NSInteger
    
    func saveFormSinister(){
   
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoadingView") as! LoadingViewController
        popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        present(popOverVC, animated: true)
        
        var intDoc = 0
        if (docTypeText.text! == "Cedula de ciudadania") {
            intDoc = 31
        }
        if (docTypeText.text! == "Cedula de extranjeria.") {
            intDoc = 32
        }
        if (docTypeText.text! == "Tarjeta de identidad") {
            intDoc = 30
        }

        let dateAsString = "\(self.calendarText.text!) \(self.hourTextField.text!)"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy h:mm a"
        var date = dateFormatter.date(from: dateAsString)
        
        let datesFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        var formdate = dateFormatter.string(from: date!)

        let datesFormattertime = DateFormatter()
        datesFormattertime.dateFormat = "yyyy-MM-dd"
        var formdatetime = datesFormattertime.string(from: date!)

            let urlString = "http://192.168.3.20:8090/api/siniestro"
        var sendCroquis = ""
        var frontal = ""
        var lateralDerecha = ""
        var lateralIzquierda = ""
        var libre = ""
        var trasera = ""
        var cedulaFrontal = ""
        var cedulaTrasera = ""
        var licenciaFrontal = ""
         var licenciaTrasera = ""
        var tarjetaPropiedadFrontal =  ""
        var tarjetaPropiedadTrasera = ""
        if(CameraCarViewController.camCarClass.photoCroquis != nil){
            sendCroquis = CameraCarViewController.camCarClass.photoCroquis!.resizedTo1MB()!.toBase64(format: ImageFormat.png)!
        }
        if(CameraCarViewController.camCarClass.photoFront != nil){
            frontal = CameraCarViewController.camCarClass.photoFront!.resizedTo1MB()!.toBase64(format: ImageFormat.png)!
        }
        if(CameraCarViewController.camCarClass.photoRight != nil){
            lateralDerecha = CameraCarViewController.camCarClass.photoRight!.resizedTo1MB()!.toBase64(format: ImageFormat.png)!
        }
        if(CameraCarViewController.camCarClass.photoLeft != nil){
            lateralIzquierda = CameraCarViewController.camCarClass.photoLeft!.resizedTo1MB()!.toBase64(format: ImageFormat.png)!
        }
        
        if(CameraCarViewController.camCarClass.photoFree != nil){
            libre = CameraCarViewController.camCarClass.photoFree!.resizedTo1MB()!.toBase64(format: ImageFormat.png)!
        }
        
        if(CameraCarViewController.camCarClass.photoBack != nil){
            trasera = CameraCarViewController.camCarClass.photoBack!.resizedTo1MB()!.toBase64(format: ImageFormat.png)!
        }
        
        if(CameraViewController.camClass.photoCedulaA != nil){
            cedulaFrontal = CameraViewController.camClass.photoCedulaA!.resizedTo1MB()!.toBase64(format: ImageFormat.png)!
        }
        
        if(CameraViewController.camClass.photoCedulaB != nil){
            cedulaTrasera = CameraViewController.camClass.photoCedulaB!.resizedTo1MB()!.toBase64(format: ImageFormat.png)!
        }
        
        if(CameraViewController.camClass.photoLicenciaA != nil){
            cedulaTrasera = CameraViewController.camClass.photoLicenciaA!.resizedTo1MB()!.toBase64(format: ImageFormat.png)!
        }
        
        if(CameraViewController.camClass.photoLicenciaB != nil){
            licenciaTrasera = CameraViewController.camClass.photoLicenciaB!.resizedTo1MB()!.toBase64(format: ImageFormat.png)!
        }
        
        if(CameraViewController.camClass.photoPropiedadA != nil){
            tarjetaPropiedadFrontal = CameraViewController.camClass.photoPropiedadA!.resizedTo1MB()!.toBase64(format: ImageFormat.png)!
        }
        
        if(CameraViewController.camClass.photoPropiedadB != nil){
            tarjetaPropiedadTrasera = CameraViewController.camClass.photoPropiedadB!.resizedTo1MB()!.toBase64(format: ImageFormat.png)!
        }
        
        
        
      
        
        Alamofire.request(urlString, method: .post, parameters: ["evidenciasDTO":["croquis": sendCroquis, "frontal": frontal,  "lateralDerecha": lateralDerecha,    "lateralIzquierda": lateralIzquierda,     "libre": libre ,     "trasera": trasera],
                                                                 
                                                                 
                                                                 "documentosDTO": ["cedulaFrontal": cedulaFrontal,
                                                                                   "cedulaTrasera": cedulaTrasera,
                                                                                   "licenciaFrontal": licenciaFrontal,
                                                                                   "licenciaTrasera": licenciaTrasera,
                                                                                   "tarjetaPropiedadFrontal": tarjetaPropiedadFrontal,
                                                                                   "tarjetaPropiedadTrasera": tarjetaPropiedadTrasera],
                                                                 
                                                                 
                                                                 
                                                                 "fecha": "\(formdatetime as Any)",
            "desEvento":"\(self.flightTextView.text!)",
            "fallosVehiculo": "\(verTextView.text!)",
            "numCelular":"\(self.phoneuser.text!)",
            "direccion":"\(self.addressUser.text!)",
            "nombreConductor":"\(nameOther.text!)",
            "tipoDocumento":"\(intDoc)",
            "numDocumento":"\(numDocOther.text!)",
            "usuarioID": "\(userId!)",
            "dominiodepartamentoID": "\(DepartmentsViewController.depReg.depId)",
            "dominiomunicipioID": "\(MunicipioViewController.munReg.idMun)",
            "placa":"\(SelectPlacaViewController.RegSinister.placa_sinister)"],encoding: JSONEncoding.default, headers: nil).responseJSON {
                response in
                                                                    

                switch response.result {

                case .success(let Json):
                    print(response)
                    let response = Json.self as! NSDictionary
                    let responsetext = response.object(forKey: "success")!
                  
                    print(responsetext)
                    if(responsetext as! Bool == true){
                        popOverVC.dismiss(animated: false){
                       
                        self.performSegue(withIdentifier: "toVerifySinister", sender: (Any).self)
                        }
                        
                    }
                    
                    break
                case .failure(let error):
                

                    print(error)
                }
            }
            
        
            
    
        
    
    
    
}
    @IBAction func callPrev(_ sender: Any) {
         dialNumber(number: "0313487555")
    }
    
    func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }
    
    @IBAction func backToHome(_ sender: Any) {
        
        self.performSegue(withIdentifier: "toAskBack", sender: (Any).self)
        
    }
    
    
  
    @IBAction func editPhone(_ sender: Any) {
        if((phoneuser.text!.count<10)){
            phoneuser.lineColor = #colorLiteral(red: 0.8894612193, green: 0.1425379813, blue: 0.02812628634, alpha: 1)
            phoneuser.errorMessage = "Numero de telefono incorrecto."
        }
        else{
            phoneuser.lineColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            phoneuser.errorMessage = ""

            
        }
        if(phoneuser.text!.count > 10){
            phoneuser.text?.removeLast()
            validateFields()
            
          
        }
     
    }
    
    @IBAction func editphone(_ sender: Any) {
        if(phoneuser.text!.count > 10){
            phoneuser.text?.removeLast()
            validateFields()
            
            if((phoneuser.text!.count<10)){
                phoneuser.lineColor = #colorLiteral(red: 0.8894612193, green: 0.1425379813, blue: 0.02812628634, alpha: 1)
            }
            else{
                phoneuser.lineColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
                
            }
        }
    }
    
    
    
    @IBAction func editIdother(_ sender: Any) {
        
        if(numDocOther.text!.count > 15){
            numDocOther.text?.removeLast()
        }
        if((numDocOther.text!.count<7)||(numDocOther.text!.count>13)){
            numDocOther.lineColor = #colorLiteral(red: 0.8894612193, green: 0.1425379813, blue: 0.02812628634, alpha: 1)
        }
        else{
            numDocOther.lineColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            
        }
    }
    
    @IBAction func editnameChange(_ sender: Any) {
        
        if(nameOther.text!.count > 50){
            nameOther.text?.removeLast()
        }

    }
    
    @IBAction func editCalendar(_ sender: Any) {
        validateFields()

    }
    
    func validateFields(){
        
        if((calendarText.text! != "")&&(hourTextField.text! != "")&&(phoneuser.text!.count >= 10)&&(addressUser.text! != "")&&(flightTextView.text! != "")&&(verTextView.text! != "")&&(deptLabel.text! != "Departamento")&&(munLabel.text! != "Municipio")){
            buttRegSinister.isEnabled = true
            buttRegSinister.backgroundColor = #colorLiteral(red: 0.4676477313, green: 0.7703307867, blue: 0.3479424715, alpha: 1)
            
        }
        else{
           buttRegSinister.isEnabled = false
            buttRegSinister.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        }
    }
    var flagsinisterDocs = false
    @IBAction func heightDocs(_ sender: Any) {
        
        if(flagsinisterDocs == false){
            heightDocuments.constant = 0
            viewDocuments.alpha = 0
            flagsinisterDocs = true
            if((heighSinister.constant == 0)&&(heightEvidencias.constant == 0)){
                heightEvidencias.constant = 350
                viewEvidencias.alpha = 1
                flagsinisterEvidencias = false
                
                
            }
        }
        else{
            
            heightDocuments.constant = 320
            viewDocuments.alpha = 1

            flagsinisterDocs = false
        }
        
    }
    
     var flagsinisterEvidencias = false
    @IBAction func heightEvidencias(_ sender: Any) {
        
        if(flagsinisterEvidencias == false){
            heightEvidencias.constant = 0
            viewEvidencias.alpha = 0
            flagsinisterEvidencias = true
            if((heighSinister.constant == 0)&&(heightDocuments.constant == 0)){
                heighSinister.constant = 818
                formViewInfoVehicle.alpha = 1
                flagsinisterForm = false

                
            }
        }
        else{
            
            heightEvidencias.constant = 350
            viewEvidencias.alpha = 1
            
            flagsinisterEvidencias = false
        }

    }
    
    @IBOutlet weak var heightCroquis: NSLayoutConstraint!
    
    @IBOutlet weak var crockisValue: UISwitch!
    
    @IBAction func corquisCheck(_ sender: Any) {
        if(crockisValue.isOn){
            heightCroquis.constant = 87
            if(CameraCarViewController.camCarClass.photoCroquis != nil){
            deleteCroquisCar.isHidden = false
            }
            else{
                deleteCroquisCar.isHidden = true

            }
        }
        else{
            CameraCarViewController.camCarClass.photoCroquis = UIImage(contentsOfFile: "")
            PhotoCroquisCar.image = #imageLiteral(resourceName: "btn_ev_croquis-1")
            heightCroquis.constant = 0
            deleteCroquisCar.isHidden = true

            
          
        }
    }
    
    
    @IBAction func PhotoPropiedadA(_ sender: Any) {
        CameraViewController.camClass.labelCamText = "Toma la foto de la parte frontal"
        GalleyViewController.galeryClass.indexPhoto = 0
        
        if(CameraViewController.camClass.photoPropiedadA != nil){
            
            self.performSegue(withIdentifier: "toGallery", sender: (Any).self)
        }
        else{
        CameraViewController.camClass.selectPropiedadA = true
        self.performSegue(withIdentifier: "toTakePhoto", sender: (Any).self)
        }
    }
    
    
    @IBAction func PhotoPolix(_ sender: Any) {
        
        CameraViewController.camClass.labelCamText = "Toma la foto de la parte frontal"
        GalleyViewController.galeryClass.indexPhoto = 2

        if(CameraViewController.camClass.photoCedulaA != nil){
            
    self.performSegue(withIdentifier: "toGallery", sender: (Any).self)
        }
        else{
            CameraViewController.camClass.selectCedulaA = true
            self.performSegue(withIdentifier: "toTakePhoto", sender: (Any).self)
        }
    }
    
    
    @IBAction func PhotoLicenciaA(_ sender: Any) {
        GalleyViewController.galeryClass.indexPhoto = 4
CameraViewController.camClass.labelCamText = "Toma la foto de la parte frontal"
        if(CameraViewController.camClass.photoLicenciaA != nil){
            
            self.performSegue(withIdentifier: "toGallery", sender: (Any).self)
        }
        else{
            CameraViewController.camClass.selectLicenciaA = true
            self.performSegue(withIdentifier: "toTakePhoto", sender: (Any).self)
        }
    }
    
    
    @IBAction func PhotoPropiedadB(_ sender: Any) {
        CameraViewController.camClass.labelCamText = "Toma la foto de la parte trasera"
        GalleyViewController.galeryClass.indexPhoto = 1

        if(CameraViewController.camClass.photoPropiedadB != nil){
            
            self.performSegue(withIdentifier: "toGallery", sender: (Any).self)
        }
        else{
            CameraViewController.camClass.selectPropiedadB = true
            self.performSegue(withIdentifier: "toTakePhoto", sender: (Any).self)
        }
    }
    
    
    @IBAction func PhotoCedulaB(_ sender: Any) {
        CameraViewController.camClass.labelCamText = "Toma la foto de la parte trasera"
        GalleyViewController.galeryClass.indexPhoto = 3

        
        if(CameraViewController.camClass.photoCedulaB != nil){
            
            self.performSegue(withIdentifier: "toGallery", sender: (Any).self)
        }
        else{
            CameraViewController.camClass.selectCedulaB = true
            self.performSegue(withIdentifier: "toTakePhoto", sender: (Any).self)
        }
        
        
    }
    
    @IBAction func PhotoLicenciaB(_ sender: Any) {
        CameraViewController.camClass.labelCamText = "Toma la foto de la parte trasera"
        GalleyViewController.galeryClass.indexPhoto = 5

        
        if(CameraViewController.camClass.photoLicenciaB != nil){
            
            self.performSegue(withIdentifier: "toGallery", sender: (Any).self)
        }
        else{
            CameraViewController.camClass.selectLicenciaB = true
            self.performSegue(withIdentifier: "toTakePhoto", sender: (Any).self)
        }
    }
    
    
    @IBAction func toDeps(_ sender: Any) {
    
        if(DepartmentsViewController.depReg.depName != ""){
        self.performSegue(withIdentifier: "toDepts", sender: (Any).self)
        }
    }
    
    
    @IBAction func todeptsFirst(_ sender: Any) {
        
        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
             self.performSegue(withIdentifier: "toDeptSegue", sender: (Any).self)
        }
        else{
       
        self.performSegue(withIdentifier: "toErrorConnect", sender: sender)
        }
        
    }
    
    @IBAction func PhotoLeftCar(_ sender: Any) {
        
         CameraCarViewController.camCarClass.labelCarCamText = "Toma la foto de la parte lateral izquierda"
        GalleryCarViewController.galeryClass.indexPhotoCar = 0

        if(CameraCarViewController.camCarClass.photoLeft != nil){
            
            self.performSegue(withIdentifier: "toGalleryCar", sender: (Any).self)
        }
        else{
        CameraCarViewController.camCarClass.maskValue = 0
        self.performSegue(withIdentifier: "toTakePhotoCar", sender: (Any).self)
        }
        
    }
    
    @IBAction func PhotoFrontCar(_ sender: Any) {
        
        CameraCarViewController.camCarClass.labelCarCamText = "Toma la foto de la parte delantera"
        
        GalleryCarViewController.galeryClass.indexPhotoCar = 1

        if(CameraCarViewController.camCarClass.photoFront != nil){
            
            self.performSegue(withIdentifier: "toGalleryCar", sender: (Any).self)
        }
        else{
        CameraCarViewController.camCarClass.maskValue = 1
        self.performSegue(withIdentifier: "toTakePhotoCar", sender: (Any).self)
        }
    }
    
    @IBAction func PhotoRightCar(_ sender: Any) {
        
         CameraCarViewController.camCarClass.labelCarCamText = "Toma la foto de la parte lateral derecha"
        
        GalleryCarViewController.galeryClass.indexPhotoCar = 2

        if(CameraCarViewController.camCarClass.photoRight != nil){
            
            self.performSegue(withIdentifier: "toGalleryCar", sender: (Any).self)
        }
        else{
        CameraCarViewController.camCarClass.maskValue = 2
        self.performSegue(withIdentifier: "toTakePhotoCar", sender: (Any).self)
        }
        
    }
    
    @IBAction func PhotoBackCar(_ sender: Any) {
        CameraCarViewController.camCarClass.labelCarCamText = "Toma la foto de la parte trasera"
        GalleryCarViewController.galeryClass.indexPhotoCar = 3

        if(CameraCarViewController.camCarClass.photoBack != nil){
            
            self.performSegue(withIdentifier: "toGalleryCar", sender: (Any).self)
        }
        else{
        CameraCarViewController.camCarClass.maskValue = 3
        self.performSegue(withIdentifier: "toTakePhotoCar", sender: (Any).self)
        }
        
    }
    
    @IBAction func PhotoFreeCar(_ sender: Any) {
        GalleryCarViewController.galeryClass.indexPhotoCar = 4

        if(CameraCarViewController.camCarClass.photoFree != nil){
            
            self.performSegue(withIdentifier: "toGalleryCar", sender: (Any).self)
        }
        else{
        CameraCarViewController.camCarClass.maskValue = 4
        self.performSegue(withIdentifier: "toTakePhotoCar", sender: (Any).self)
        }
    }
    
    @IBAction func PhotoCroquisCar(_ sender: Any) {
        GalleryCarViewController.galeryClass.indexPhotoCar = 5

        if(CameraCarViewController.camCarClass.photoCroquis != nil){
            
            self.performSegue(withIdentifier: "toGalleryCar", sender: (Any).self)
        }
        else{
        CameraCarViewController.camCarClass.maskValue = 5
        self.performSegue(withIdentifier: "toTakePhotoCar", sender: (Any).self)
        }
    }
    
    
    
    
    @IBAction func phoneChanged(_ sender: Any) {
        self.validateFields()
    }
    
    @IBAction func addressChanged(_ sender: Any) {
        self.validateFields()

    }
    
    @IBAction func hourChanged(_ sender: Any) {
        self.validateFields()

    }
    
    @IBAction func CalendarChanged(_ sender: Any) {
        self.validateFields()

    }
    
    func textViewDidChangeFlight(_ textView: UITextView) {
        switch (textView) {
        case flightTextView:
            validateFields()
        break
            
        case verTextView:
            validateFields()
            break
        default: break
        }
    }
    
    @IBAction func toSetPlaca(_ sender: Any) {
        
        print(HomeViewController.homeClass.uniquePlaca)
        if(HomeViewController.homeClass.uniquePlaca != 1){
        self.performSegue(withIdentifier: "toSetPlaca", sender: sender)
        }
    }
    
    
    
    
}




public enum ImageFormat {
    case png
    case jpeg(CGFloat)
}

extension UIImage {
    public func toBase64(format: ImageFormat) -> String? {
        var imageData: Data?
        
        switch format {
        case .png:
            imageData = self.pngData()
        case .jpeg(let compression):
            imageData = self.jpegData(compressionQuality: compression)
        
        }
        
        return imageData?.base64EncodedString()
    }
    
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func resizedTo1MB() -> UIImage? {
        guard let imageData = self.pngData() else { return nil }
        let megaByte = 1000.0
        
        var resizingImage = self
        var imageSizeKB = Double(imageData.count) / megaByte // ! Or devide for 1024 if you need KB but not kB
        
        while imageSizeKB > megaByte { // ! Or use 1024 if you need KB but not kB
            guard let resizedImage = resizingImage.resized(withPercentage: 0.5),
                let imageData = resizedImage.pngData() else { return nil }
            
            resizingImage = resizedImage
            imageSizeKB = Double(imageData.count) / megaByte // ! Or devide for 1024 if you need KB but not kB
        }
        
        return resizingImage
    }
    
    

}




extension UITextView :UITextViewDelegate
{
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.count > 0
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
}
