//
//  PopUpOkViewController.swift
//  autogestion
//
//  Created by Asesoftware on 9/11/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit

class PopUpOkViewController: UIViewController {

    @IBOutlet weak var messageOk: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        messageOk.text = "Haz agregado el vehículo con placas \(AddCarViewController.carVar.placa)."

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func closePopUp(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
}
