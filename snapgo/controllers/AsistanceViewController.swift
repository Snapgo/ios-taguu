//
//  AsistanceViewController.swift
//  autogestion
//
//  Created by Asesoftware on 10/28/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AsistanceViewController: UIViewController {

    var numberPlomeria = ""
    var numberOtros = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        getPhoneNumbersAssitance()
        
    }
    
    @IBAction func Call_plomeria(_ sender: Any) {
        dialNumber(number: numberPlomeria)

    }
    
    @IBAction func Call_Otros(_ sender: Any) {
        dialNumber(number: numberOtros)

    }
    
    
    
    func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
        }
    }
    
    func getPhoneNumbersAssitance() {
        
        
        let urlString = "http://192.168.3.20:8090/api/parametros/AAPM_PARAM_008,AAPM_PARAM_007"
        
        Alamofire.request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success(let Json):
                print(response)
                let response = Json.self as! NSDictionary
                let responseNumber = response.object(forKey: "data")
                let responseNumbers =  (responseNumber as AnyObject).object(forKey: "parameters")!
                
                var rewponseN = responseNumbers as! NSDictionary
                self.numberPlomeria = rewponseN.object(forKey: "AAPM_PARAM_007") as! String
                self.numberOtros = rewponseN.object(forKey: "AAPM_PARAM_008") as! String
           
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
