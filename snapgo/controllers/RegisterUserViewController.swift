//
//  RegisterUserViewController.swift
//  autogestion
//
//  Created by Asesoftware on 9/5/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit
import MaterialComponents
import Alamofire
import SwiftyJSON
import SkyFloatingLabelTextField
import EzPopup

protocol DimissManager {
    func vcDismissed()
}

class RegisterUserViewController: UIViewController, UITextFieldDelegate, communicationControllerCamera{
    
    func backFromCamera() {
        print("Back from camera")

    }
    
    
  
    func vcDismissed() {
        print("Dismissed")
    }
    
    @IBOutlet weak var passErrorField: UILabel!
    @IBOutlet weak var nameField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var docList: MDCTextField!
    
    @IBOutlet weak var docNumber: MDCTextField!
    
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var passField: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmPassField: SkyFloatingLabelTextField!
    @IBOutlet weak var registerButt: UIButton!
    var flagCheck = false
    
    @IBOutlet weak var ayePassImg: UIButton!
    @IBOutlet weak var eyePassConfImg: UIButton!
    
    var eyeFlag = false
    var eyeFlagConf = false
    @IBAction func eyePass(_ sender: Any) {
        if(eyeFlag == false){
            
            ayePassImg.setImage(#imageLiteral(resourceName: "ico_contrasena1"), for: .normal)
         self.eyeFlag = true
            passField.isSecureTextEntry = false
        }
        else{
            ayePassImg.setImage(#imageLiteral(resourceName: "ico_contrasena"), for: .normal)
            self.eyeFlag = false
            passField.isSecureTextEntry = true

        }
    }
    
    @IBAction func eyePassConf(_ sender: Any) {
        if(eyeFlagConf == false){
            
            eyePassConfImg.setImage(#imageLiteral(resourceName: "ico_contrasena1"), for: .normal)
            self.eyeFlagConf = true
            confirmPassField.isSecureTextEntry = false
        }
        else{
            eyePassConfImg.setImage(#imageLiteral(resourceName: "ico_contrasena"), for: .normal)
            self.eyeFlagConf = false
            confirmPassField.isSecureTextEntry = true

        }
    }
    
    struct RegVar {
        static var ListVal = ""
    }
    

    
    weak var vc:DoctypeViewController? =  DoctypeViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        vc?.delegate = self

        
        
        registerButt?.isEnabled = false
        passErrorField?.alpha = 0
    
        
        nameField?.font = UIFont(name: "Neris-Light", size: 17)
        lastNameField?.font = UIFont(name: "Neris-Light", size: 17)
        docList?.font  = UIFont(name: "Neris-Light", size: 17)
        docNumber?.font  = UIFont(name: "Neris-Light", size: 17)
        emailField?.font  = UIFont(name: "Neris-Light", size: 17)
        phoneField?.font  = UIFont(name: "Neris-Light", size: 17)
        passField?.font  = UIFont(name: "Neris-Light", size: 17)
        confirmPassField?.font  = UIFont(name: "Neris-Light", size: 17)
        
        // Do any additional setup after loading the view.
    }
    var flagPass = false
    @IBAction func beginEditPass(_ sender: Any) {
        if flagPass == false {
            let customAlertVC = storyboard!.instantiateViewController(withIdentifier: "PopView")
            let popupVC = PopupViewController(contentController: customAlertVC, popupWidth: 100, popupHeight: 200)
            present(popupVC, animated: true)

        }
    }
    
    @IBAction func registerButt(_ sender: Any) {
        
        
        
        
        
        if Connectivity.isConnectedToInternet() {
            print("Yes! internet is available.")
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoadingView") as! LoadingViewController
            popOverVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            present(popOverVC, animated: true)
            
        
        let urlString = "http://192.168.3.20:8080/api/authentication/signup"
        
        Alamofire.request(urlString, method: .post, parameters: ["nombres": "\(nameField.text!)","apellidos":"\(lastNameField.text!)","celular":"\(phoneField.text!)","tipoDoc":"\(docList.text!)","numDoc":"\(docNumber.text!)","email":"\(emailField.text!)","password":"\(passField.text!)"],encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success(let Json):
                print(response)
                let response = Json.self as! NSDictionary
                let responsetext = response.object(forKey: "success")!
                
                print(responsetext)
                if(responsetext as! Bool == true){
                    popOverVC.dismiss(animated: false){

                    self.performSegue(withIdentifier: "toLoginfromReg", sender: sender)
                    }
                }
                if(responsetext as! Bool == false){
                    popOverVC.dismiss(animated: false){

                    self.performSegue(withIdentifier: "toErrorUser", sender: sender)
                    }
                }
                
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        }
        else{
            self.performSegue(withIdentifier: "toErrorConnect", sender: sender)
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBOutlet weak var checkImage: UIImageView!
    @IBAction func checkTerms(_ sender: Any) {
        if (flagCheck == false) {
             checkImage.image = #imageLiteral(resourceName: "check1")
            flagCheck = true
        }
        else{
            checkImage.image = #imageLiteral(resourceName: "check")
            flagCheck = false
        }
        print("validate")
        
        var passVal = isValidPass(passStr: passField.text!)
        var emailVal = isValidEmail(emailStr: emailField.text!)
        if((nameField.text! != "")&&(lastNameField.text! != "")&&(docList.text! != "")&&(docNumber.text! != "")&&(emailField.text! != "")&&(phoneField.text! != "")&&(passField.text! != "")&&(confirmPassField.text! != "")&&(passField.text! == confirmPassField.text!)&&(emailVal)&&(passVal)&&(flagCheck == true)){
            
            
            registerButt.isEnabled = true
            registerButt.backgroundColor = #colorLiteral(red: 0.4676477313, green: 0.7703307867, blue: 0.3479424715, alpha: 1)
        }
        else{
            registerButt.isEnabled = false
            registerButt.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
       
    }
    
    
    
    @IBAction func passEdit(_ sender: Any) {
        if(passField.text!.count > 30){
            passField.text?.removeLast()
        }
        if(isValidPass(passStr: passField.text!)){
            passField.lineColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            passField.errorMessage = ""

            
        }
        else{
            passField.lineColor = #colorLiteral(red: 0.8894612193, green: 0.1425379813, blue: 0.02812628634, alpha: 1)
            
            
            passField.errorMessage = "Contraseña inválida"
            
            
            
        }
    }
    
    
    @IBAction func pass2Edit(_ sender: Any) {
        
        if(confirmPassField.text!.count > 30){
            confirmPassField.text?.removeLast()
        }

        if(isValidPass(passStr: confirmPassField.text!)){
            confirmPassField.lineColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            confirmPassField.errorMessage = ""
            

            
        }
        else{
            confirmPassField.lineColor = #colorLiteral(red: 0.8894612193, green: 0.1425379813, blue: 0.02812628634, alpha: 1)
            
            confirmPassField.errorMessage = "Contraseña inválida"
            
            
        }
    }
    @IBAction func phoneEdit(_ sender: Any) {
        if(phoneField.text != ""){
        if((Array(phoneField.text!)[0] == "3")&&(phoneField.text!.count == 10)){
            phoneField.lineColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            
            phoneField.errorMessage = ""

        }
        else{
            phoneField.lineColor = #colorLiteral(red: 0.8894612193, green: 0.1425379813, blue: 0.02812628634, alpha: 1)
            
            
            phoneField.errorMessage = "Celular Inválido"
            

        }
        }
            
    }
    
    @IBAction func editemail(_ sender: Any) {
        if(isValidEmail(emailStr: emailField.text!)){
            emailField.lineColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            emailField.errorMessage = ""
            
        }
        else{
            emailField.lineColor = #colorLiteral(red: 0.8894612193, green: 0.1425379813, blue: 0.02812628634, alpha: 1)
           

           
            emailField.errorMessage = "Email Inválido"
            

        }
    }
    
    @IBAction func editDocNum(_ sender: Any) {
        if((docNumber.text!.count<7)||(docNumber.text!.count>13)){
            docNumber.underline?.color = #colorLiteral(red: 0.8894612193, green: 0.1425379813, blue: 0.02812628634, alpha: 1)
            docNumber.leadingUnderlineLabel.isHidden = false
            
            docNumber.leadingUnderlineLabel.text = "Número Inválido"
            docNumber.leadingUnderlineLabel.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)

        }
        else{
            docNumber.underline?.color = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            docNumber.leadingUnderlineLabel.isHidden = true

            
            

        }
    }
    
 
    
    
    
    @IBAction func validname(_ sender: MDCTextField) {
        print("validate")
        
       
        
        var passVal = isValidPass(passStr: passField.text!)
        var emailVal = isValidEmail(emailStr: emailField.text!)
        if((nameField.text! != "")&&(lastNameField.text! != "")&&(docList.text! != "")&&(docNumber.text! != "")&&(emailField.text! != "")&&(phoneField.text! != "")&&(passField.text! != "")&&(confirmPassField.text! != "")&&(passField.text! == confirmPassField.text!)&&(emailVal)&&(passVal)&&(flagCheck == true)){
            
            
            registerButt.isEnabled = true
            registerButt.backgroundColor = #colorLiteral(red: 0.4676477313, green: 0.7703307867, blue: 0.3479424715, alpha: 1)
        }
        else{
            registerButt.isEnabled = false
            registerButt.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }

    }
    
    
    
    
    func isValidNames(nameStr:String) -> Bool {
        let emailRegEx = "[a-zA-ZÀ-ÿ\\s]+"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: nameStr)
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    func isValidPass(passStr:String) -> Bool {
        let passRegEx = "^(?=.*[A-Z])(?=.*[!@#$&]*)(?=.*[0-9].*[0-9])(?=.*[a-z]).{3,25}$"
        
        let passPred = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        print(passPred.evaluate(with: passStr))
        return passPred.evaluate(with: passStr)
    }
    var list_Text = ""

    override func viewDidAppear(_ animated: Bool) {
        self.docList?.text =  UserDefaults.standard.string(forKey: "typedoc")
       
    }
    override func viewDidDisappear(_ animated: Bool) {
    }
    override func viewWillAppear(_ animated: Bool) {
    }
    override func viewWillDisappear(_ animated: Bool) {

        viewDidAppear(true)
    }
    
    override func viewDidLayoutSubviews() {
        self.docList?.text =  UserDefaults.standard.string(forKey: "typedoc")

    }
    

   
    
    @IBAction func close_PopUp(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func changeListDon(value: String){
        RegVar.ListVal = value
    }
    
    
    @IBAction func toDocType(_ sender: Any) {
      /*
        let customAlertVC = storyboard!.instantiateViewController(withIdentifier: "DoctypeViewController")
        var docType = DoctypeViewController()
        docType.delegate = self
        
        let popupVC = PopupViewController(contentController: customAlertVC, popupWidth: 100, popupHeight: 200)
        
        present(popupVC, animated: true)*/
        let popup  = self.storyboard?.instantiateViewController(withIdentifier: "DoctypeViewController") as! DoctypeViewController
        popup.modalPresentationStyle = .currentContext
       self.present(popup, animated: false)
        
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
        if let coordinator = navigationController.topViewController?.transitionCoordinator {
            coordinator.notifyWhenInteractionChanges { (context) in
                print("Is cancelled: \(context.isCancelled)")
            }
        }
    }
 
    @IBAction func toTerms(_ sender: Any) {
       
        self.performSegue(withIdentifier: "toTerms&", sender: (Any).self)
    }
    
    
    @IBAction func editingPhone(_ sender: Any) {
    
       
    }
 
 
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return range.location < 50
    }
    
    
    @IBAction func changedEditForm(_ sender: Any) {
        
        
        if(isValidNames(nameStr: nameField.text!)){
            nameField.lineColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            
            nameField.errorMessage = ""
        
        }
        else{
          
            nameField.lineColor = #colorLiteral(red: 0.8894612193, green: 0.1425379813, blue: 0.02812628634, alpha: 1)
            
            nameField.errorMessage = "Nombre inválido"
        }
        validate()

    }
    
    
    @IBAction func testcontrol(_ sender: Any) {
        
        print("validate")

    }
    
    @IBAction func tested(_ sender: MDCTextField) {
        if(nameField.text!.count > 50){
            nameField.text?.removeLast()

        }
       validate()

    }
    
    @IBAction func editingLastName(_ sender: Any) {
        if(lastNameField.text!.count > 50){
            lastNameField.text?.removeLast()
        }
        if(isValidNames(nameStr: lastNameField.text!)){
            lastNameField.lineColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            
            lastNameField.errorMessage = ""
            
        }
        else{
            
            lastNameField.lineColor = #colorLiteral(red: 0.8894612193, green: 0.1425379813, blue: 0.02812628634, alpha: 1)
            
            lastNameField.errorMessage = "Apellidos inválidos"
        }
        validate()
    }
    
    
    @IBAction func editingTypeDoc(_ sender: Any) {
        validate()
    }
    
    @IBAction func editingNumDoc(_ sender: Any) {
        if(docNumber.text!.count > 12){
            docNumber.text?.removeLast()
        }
        
        validate()
    }
    
    @IBAction func editingEmail(_ sender: Any) {
        if(emailField.text!.count > 50){
            emailField.text?.removeLast()
        }
        validate()
    }
    
    @IBAction func editingPhones(_ sender: Any) {
        if(phoneField.text!.count > 10){
            phoneField.text?.removeLast()
        }
        validate()
    }
    
    @IBAction func editingPass(_ sender: Any) {
        if(passField.text!.count > 30){
            passField.text?.removeLast()
        }
        if(passField.text! == confirmPassField.text!){
            self.passErrorField.alpha = 0
        }
        else{
            self.passErrorField.alpha = 1
            passField.lineColor = #colorLiteral(red: 0.8894612193, green: 0.1425379813, blue: 0.02812628634, alpha: 1)

        }
        validate()
    }
    
    @IBAction func editingpassTwo(_ sender: Any) {
        if(confirmPassField.text!.count > 30){
            confirmPassField.text?.removeLast()
        }
        if(passField.text! == confirmPassField.text!){
            self.passErrorField.alpha = 0
        }
        else{
            self.passErrorField.alpha = 1
            confirmPassField.lineColor = #colorLiteral(red: 0.8894612193, green: 0.1425379813, blue: 0.02812628634, alpha: 1)

            
        }
        validate()
    }
    
    
    
    func validate(){
        
        print("validate")
        
       
        
        var passVal = isValidPass(passStr: passField.text!)
        var emailVal = isValidEmail(emailStr: emailField.text!)
        if((nameField.text! != "")&&(lastNameField.text! != "")&&(docList.text! != "")&&(docNumber.text!.count>6)&&(emailField.text! != "")&&(phoneField.text!.count  == 10)&&(passField.text! != "")&&(confirmPassField.text! != "")&&(passField.text! == confirmPassField.text!)&&(emailVal)&&(passVal)&&(flagCheck == true)){
            
            
            registerButt.isEnabled = true
            registerButt.backgroundColor = #colorLiteral(red: 0.4676477313, green: 0.7703307867, blue: 0.3479424715, alpha: 1)
        }
        else{
            registerButt.isEnabled = false
            registerButt.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }

    }
    
    func valid(){
        print("ok")
    }
    
    
    @IBAction func hideDoc(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func ccSelect(_ sender: Any) {
       
        
      //  UserDefaults.standard.set("Cédula de ciudadanía", forKey: "typedoc") //setObject
        UserDefaults.standard.set("Cédula de ciudadania.", forKey: "typedoc") //setObject
       // self.docList.text! = "Cédula de ciudadanía"
        self.dismiss(animated: false, completion: nil)

        
    }
    
    @IBAction func ceSelect(_ sender: Any) {
        UserDefaults.standard.set("Cédula de extranjería.", forKey: "typedoc") //setObject
        self.dismiss(animated: false, completion: nil)

        
    }
    
    @IBAction func nitSelect(_ sender: Any) {
        UserDefaults.standard.set("Tarjeta de identidad", forKey: "typedoc") //setObject
        self.dismiss(animated: false, completion: nil)

        
        
    }
    

    var x = 0
    typealias Action = (_ x: DoctypeViewController) -> () // replace AnyObject to what you need

    func modalAction() -> Action {
        return { [unowned self] x in
            print("hi")
        }
    }
    
    
    
}


extension UIViewController {
    func presentOnRoot(`with` viewController : UIViewController){
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.currentContext
        self.present(navigationController, animated: false, completion: nil)
    }
 
    
    
}

