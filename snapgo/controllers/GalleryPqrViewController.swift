//
//  GalleryPqrViewController.swift
//  autogestion
//
//  Created by Asesoftware on 11/6/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit
import ImageSlideshow


class GalleryPqrViewController: UIViewController {
    @IBOutlet weak var sliderImage: ImageSlideshow!

    struct galeryClass {
        static var deleteAux = false
        static var indexPhoto = 0
        static var indexExist : [Bool]? = [false, false,false]
    }
    var imagesources: [ImageSource] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .portrait
        
        
        if( CameraPqrViewController.camPqrClass.photo1 != nil){
            imagesources.append( ImageSource(image: CameraPqrViewController.camPqrClass.photo1!
            ))
        }
        if(( CameraPqrViewController.camPqrClass.photo2 != nil)){
            imagesources.append( ImageSource(image: CameraPqrViewController.camPqrClass.photo2!
            ))
        }
        if( CameraPqrViewController.camPqrClass.photo3 != nil){
            imagesources.append( ImageSource(image: CameraPqrViewController.camPqrClass.photo3!
            ))
        }
    
        
        sliderImage?.setImageInputs(imagesources)
        sliderImage?.setCurrentPage(galeryClass.indexPhoto, animated: true)
        // Do any additional setup after loading the view.
        
    }
    
    
    
    @IBAction func closeGallery(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    
    
    func deletePhoto() {
        
        var photId = [Int]()
        
        for index in 0...sliderImage.currentPage{
            photId.removeAll()
            var aux  = 0
            for idexphoto in 0...GalleryPqrViewController.galeryClass.indexExist!.count-1{
                if(GalleryPqrViewController.galeryClass.indexExist![idexphoto] == true){
                    photId.append(idexphoto)
                    
                }
                
            }
        }
        print(imagesources.count)
        print(sliderImage!.currentPage)
        if(imagesources.count > 1){
            print(sliderImage!.currentPage)
            //print(photId[sliderImage!.currentPage])
            if(photId[sliderImage.currentPage] == 0){
                CameraPqrViewController.camPqrClass.photo1 = nil
                GalleryPqrViewController.galeryClass.indexExist![0] = false
            }
            if(photId[sliderImage.currentPage] == 1){
                CameraPqrViewController.camPqrClass.photo2 = nil
                GalleryPqrViewController.galeryClass.indexExist![1] = false
                
                
            }
            if(photId[sliderImage.currentPage] == 2){
                CameraPqrViewController.camPqrClass.photo3 = nil
                GalleryPqrViewController.galeryClass.indexExist![2] = false
                
                
            }
     
         
            
        }
        else{
            print(photId[0])
            
            if(photId[0] == 0){
                CameraPqrViewController.camPqrClass.photo1 = nil
                GalleryPqrViewController.galeryClass.indexExist![0] = false
                
                
            }
            if(photId[0] == 1){
                CameraPqrViewController.camPqrClass.photo2 = nil
                GalleryPqrViewController.galeryClass.indexExist![1] = false
                
                
            }
            if(photId[0] == 2){
                CameraPqrViewController.camPqrClass.photo3 = nil
                GalleryPqrViewController.galeryClass.indexExist![2] = false
                
            }
       
        }
        
        
        self.dismiss(animated: false, completion: nil)
        galeryClass.deleteAux = false
        
    }
    
    
    
    
    
    @IBAction func toDelete(_ sender: Any) {
        deletePhoto()
        galeryClass.deleteAux = true
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBOutlet weak var cancelPhoto: UILabel!
    
    @IBAction func cancelDelete(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("dfdf")
        if(galeryClass.deleteAux == true){
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        print("srgrg")
    }
}



