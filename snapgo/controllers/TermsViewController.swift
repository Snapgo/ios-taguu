//
//  TermsViewController.swift
//  autogestion
//
//  Created by Asesoftware on 9/9/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TermsViewController: UIViewController {

    @IBOutlet weak var textTerms: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let urlString = "http://192.168.3.20:8090/api/tooltips/tyc"
        
        Alamofire.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success(let Json):
                print(response)
                let response = Json.self as! NSDictionary
                
                let responsetext = response.object(forKey: "data")!
                
                var rewponseN = responsetext as! NSDictionary
                var responsetext2 = rewponseN.object(forKey: "TyC") as! String
                
                print(responsetext)
                self.textTerms.text = "\(responsetext2)"
                
                
                
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func toRegister(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
