//
//  AddCarViewController.swift
//  autogestion
//
//  Created by Asesoftware on 9/10/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class AddCarViewController: UIViewController, UITextFieldDelegate {
    
    struct carVar {
        static var placa = ""
        static var validPlaca = false
        static var messagePlaca = ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 6

        let currentString: NSString = textField.text!.uppercased() as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }

    @IBOutlet weak var placaField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        placaField.delegate = self
        placaField.autocapitalizationType = .allCharacters
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeView(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func nullToNil(value : Any?) -> Any? {
        if value is NSNull {
            return nil
        } else {
            return value
        }
    }
    
    @IBAction func regCar(_ sender: Any) {
        
        
         var userId: Int? = UserDefaults.standard.object(forKey: "UserID") as? Int
        let urlString = "http://192.168.3.20:8090/api/poliza/registrarPlaca"
        
        Alamofire.request(urlString, method: .post, parameters: ["placa": "\(placaField.text!)", "usuarioID": "\(userId!)"],encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success(let Json):
                print(response)
                let response = Json.self as! NSDictionary
                let responseVehicles = response.object(forKey: "data")

                let responsetext = response.object(forKey: "success")!
                let responseMessage = response.object(forKey: "message")
                
                carVar.messagePlaca = responseMessage as! String

             
                print(responsetext)
                if((self.nullToNil(value: responseVehicles) == nil)&&(responsetext as! Bool == false))
                {
                    self.performSegue(withIdentifier: "toPlacaprevAdded", sender: sender)
                    
                }
                else{
                
                if(responsetext as! Bool == true){
                    
                    carVar.placa = self.placaField.text!
                    self.performSegue(withIdentifier: "toOkfromPlaca", sender: sender)
                    
                    carVar.validPlaca = true
                    let defaults = UserDefaults.standard
                    defaults.set("true", forKey: "userPlaca")
                    
                }
                else{
                   self.performSegue(withIdentifier: "toErrorFromPlaca", sender: sender)
                }
                
                }
                
                break
            case .failure(let error):
                
                print(error)
            }
        }
        
        
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
