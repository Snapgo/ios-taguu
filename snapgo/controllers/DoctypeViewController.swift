//
//  DoctypeViewController.swift
//  autogestion
//
//  Created by Asesoftware on 9/11/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit

protocol communicationControllerCamera {
    func backFromCamera()
}


class DoctypeViewController: UIViewController {

    var delegate: communicationControllerCamera? = nil


    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate?.backFromCamera()


        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func hideDoc(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func ccSelect(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)

        
       UserDefaults.standard.set("Cédula de ciudadanía", forKey: "typedoc") //setObject
        
     
    
    }
    
    @IBAction func ceSelect(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        UserDefaults.standard.set("Cédula de extranjería.", forKey: "typedoc") //setObject
        
        
    }
    
    @IBAction func nitSelect(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        UserDefaults.standard.set("Tarjeta de identidad", forKey: "typedoc") //setObject
        
        
    }
    
   
  
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
