//
//  GalleryCarViewController.swift
//  autogestion
//
//  Created by Asesoftware on 10/2/19.
//  Copyright © 2019 Asesoftware. All rights reserved.
//

import UIKit
import ImageSlideshow

class GalleryCarViewController: UIViewController {
    
    @IBOutlet weak var sliderImage: ImageSlideshow!
    
    struct galeryClass {
        static var carDeleteAux = false
        static var indexPhotoCar = 0
        static var indexExistCar : [Bool]? = [false, false,false, false,false, false]
    }
    var imagesourcesCar: [ImageSource] = []
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .portrait
        
        
        if( CameraCarViewController.camCarClass.photoLeft != nil){
            imagesourcesCar.append( ImageSource(image: CameraCarViewController.camCarClass.photoLeft!
            ))
        }
        if( CameraCarViewController.camCarClass.photoFront != nil){
            imagesourcesCar.append( ImageSource(image: CameraCarViewController.camCarClass.photoFront!
            ))
        }
        if( CameraCarViewController.camCarClass.photoRight != nil){
            imagesourcesCar.append( ImageSource(image: CameraCarViewController.camCarClass.photoRight!
            ))
        }
        if( CameraCarViewController.camCarClass.photoBack != nil){
            imagesourcesCar.append( ImageSource(image: CameraCarViewController.camCarClass.photoBack!
            ))
        }
        if( CameraCarViewController.camCarClass.photoFree != nil){
            imagesourcesCar.append( ImageSource(image: CameraCarViewController.camCarClass.photoFree!
            ))
        }
        if( CameraCarViewController.camCarClass.photoCroquis != nil){
            imagesourcesCar.append( ImageSource(image: CameraCarViewController.camCarClass.photoCroquis!
            ))
        }
        
        
        
        sliderImage?.setImageInputs(imagesourcesCar)
        sliderImage?.setCurrentPage(galeryClass.indexPhotoCar, animated: true)
        // Do any additional setup after loading the view.
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func closeGallery(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
  
    
    
    func deletePhoto() {
        
        var photId = [Int]()
        
        for index in 0...sliderImage.currentPage{
            photId.removeAll()
            var aux  = 0
            for idexphoto in 0...GalleryCarViewController.galeryClass.indexExistCar!.count-1{
                if(GalleryCarViewController.galeryClass.indexExistCar![idexphoto] == true){
                    photId.append(idexphoto)
                }
            }
        }
        print(imagesourcesCar.count)
        if(imagesourcesCar.count > 1){
            print(sliderImage.currentPage)
            print(photId[sliderImage!.currentPage])
            if(photId[sliderImage.currentPage] == 0){
                CameraCarViewController.camCarClass.photoLeft = nil
                GalleryCarViewController.galeryClass.indexExistCar![0] = false
            }
            if(photId[sliderImage.currentPage] == 1){
                CameraCarViewController.camCarClass.photoFront = nil
                GalleryCarViewController.galeryClass.indexExistCar![1] = false
                
                
            }
            if(photId[sliderImage.currentPage] == 2){
                CameraCarViewController.camCarClass.photoRight = nil
                GalleryCarViewController.galeryClass.indexExistCar![2] = false
                
                
            }
            if(photId[sliderImage.currentPage] == 3){
                CameraCarViewController.camCarClass.photoBack = nil
                GalleryCarViewController.galeryClass.indexExistCar![3] = false
                
                
            }
            if(photId[sliderImage.currentPage] == 4){
                CameraCarViewController.camCarClass.photoFree = nil
                GalleryCarViewController.galeryClass.indexExistCar![4] = false
                
                
            }
            if(photId[sliderImage.currentPage] == 5){
                CameraCarViewController.camCarClass.photoCroquis = nil
                GalleryCarViewController.galeryClass.indexExistCar![5] = false
                
                
            }
            
        }
        else{
            print(photId[0])
            
            if(photId[0] == 0){
                CameraCarViewController.camCarClass.photoLeft = nil
                GalleryCarViewController.galeryClass.indexExistCar![0] = false
                
                
            }
            if(photId[0] == 1){
                CameraCarViewController.camCarClass.photoFront = nil
                GalleryCarViewController.galeryClass.indexExistCar![1] = false
                
                
            }
            if(photId[0] == 2){
                CameraCarViewController.camCarClass.photoRight = nil
                GalleryCarViewController.galeryClass.indexExistCar![2] = false
                
            }
            if(photId[0] == 3){
                CameraCarViewController.camCarClass.photoBack = nil
                GalleyViewController.galeryClass.indexExist![3] = false
                
            }
            if(photId[0] == 4){
                CameraCarViewController.camCarClass.photoFree = nil
                GalleryCarViewController.galeryClass.indexExistCar![4] = false
                
            }
            if(photId[0] == 5){
                CameraCarViewController.camCarClass.photoCroquis = nil
                GalleryCarViewController.galeryClass.indexExistCar![5] = false
                
            }
        }
        self.dismiss(animated: false, completion: nil)
        galeryClass.carDeleteAux = false

    }
    
    
    
    
    
    @IBAction func toDelete(_ sender: Any) {
        galeryClass.carDeleteAux = true
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBOutlet weak var cancelPhoto: UILabel!
    
    @IBAction func cancelDelete(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("dfdf")
        if(galeryClass.carDeleteAux == true){
            deletePhoto()
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        print("srgrg")
    }

    
}
